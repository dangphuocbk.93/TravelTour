package travel.entity;
// Generated Apr 13, 2017 10:12:16 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Trangthaidonhang generated by hbm2java
 */
@Entity
@Table(name="trangthaidonhang"
    ,catalog="travel_tour"
)
public class Trangthaidonhang  implements java.io.Serializable {


     private Integer id;
     private String tentrangthai;
     private Set<Donhang> donhangs = new HashSet<Donhang>(0);

    public Trangthaidonhang() {
    }

	
    public Trangthaidonhang(String tentrangthai) {
        this.tentrangthai = tentrangthai;
    }
    public Trangthaidonhang(String tentrangthai, Set<Donhang> donhangs) {
       this.tentrangthai = tentrangthai;
       this.donhangs = donhangs;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    
    @Column(name="tentrangthai", nullable=false, length=100)
    public String getTentrangthai() {
        return this.tentrangthai;
    }
    
    public void setTentrangthai(String tentrangthai) {
        this.tentrangthai = tentrangthai;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="trangthaidonhang",cascade=CascadeType.ALL)
    public Set<Donhang> getDonhangs() {
        return this.donhangs;
    }
    
    public void setDonhangs(Set<Donhang> donhangs) {
        this.donhangs = donhangs;
    }




}


