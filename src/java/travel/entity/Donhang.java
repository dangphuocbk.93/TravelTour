package travel.entity;
// Generated Apr 13, 2017 10:12:16 PM by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Donhang generated by hbm2java
 */
@Entity
@Table(name = "donhang", catalog = "travel_tour"
)
public class Donhang implements java.io.Serializable {

    private Integer id;
    private Nguoidung nguoidung;
    private Trangthaidonhang trangthaidonhang;
    private Date ngaydattour;
    private String ghichu;
    private Set<Chitietdonhang> chitietdonhangs = new HashSet<Chitietdonhang>(0);

    public Donhang() {
    }

    public Donhang(Nguoidung nguoidung, Trangthaidonhang trangthaidonhang, Date ngaydattour, String ghichu, Set<Chitietdonhang> chitietdonhangs) {
        this.nguoidung = nguoidung;
        this.trangthaidonhang = trangthaidonhang;
        this.ngaydattour = ngaydattour;
        this.ghichu = ghichu;
        this.chitietdonhangs = chitietdonhangs;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_khachhang", nullable = false)
    public Nguoidung getNguoidung() {
        return this.nguoidung;
    }

    public void setNguoidung(Nguoidung nguoidung) {
        this.nguoidung = nguoidung;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_trangthai", nullable = false)
    public Trangthaidonhang getTrangthaidonhang() {
        return this.trangthaidonhang;
    }

    public void setTrangthaidonhang(Trangthaidonhang trangthaidonhang) {
        this.trangthaidonhang = trangthaidonhang;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ngaydattour", nullable = false, length = 10)
    public Date getNgaydattour() {
        return this.ngaydattour;
    }

    public void setNgaydattour(Date ngaydattour) {
        this.ngaydattour = ngaydattour;
    }

    @Column(name = "ghichu", length = 65535)
    public String getGhichu() {
        return this.ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "donhang", cascade = CascadeType.ALL)
    public Set<Chitietdonhang> getChitietdonhangs() {
        return this.chitietdonhangs;
    }

    public void setChitietdonhangs(Set<Chitietdonhang> chitietdonhangs) {
        this.chitietdonhangs = chitietdonhangs;
    }

}
