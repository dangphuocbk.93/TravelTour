/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.booking.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import travel.admin.DAO.*;
import travel.entity.*;
import travel.utility.*;

/**
 *
 * @author Phuoc
 */
@Controller("TourBooking")
@Transactional
@RequestMapping("booking/tour")
public class TourController {

    @Autowired
    TourDAO tDAO;
    @Autowired
    KhuvucDAO kvDAO;
    @Autowired
    ThuonghieuDAO thDAO;
    @Autowired
    HinhanhDAO haDAO;
    @Autowired
    Pagination pagination;

    @RequestMapping("newtour")
    public String getNewTour(ModelMap m) {
        String sql = "SELECT * FROM Tour ORDER BY ngaytao LIMIT 0,6";
        List<Tour> lstt = tDAO.getBySql(sql);
        m.addAttribute("lstt", lstt);
        return "main-content";
    }

    @RequestMapping("pagin")
    public String pagin(@RequestParam("hql") String hql, @RequestParam("page") int page, ModelMap m) {
        int limit = 4;
        List<Tour> lstt = pagination.getByPage(limit, page, tDAO.getByHql(hql));
        m.addAttribute("pagenumber", pagination.pageNumber(limit, tDAO.getByHql(hql)));
        m.addAttribute("lstt", lstt);
        m.addAttribute("page", page);
        m.addAttribute("lstkv", kvDAO.getAll());
        m.addAttribute("lstth", thDAO.getAll());
        m.addAttribute("hql", hql);
        m.addAttribute("urlQuery", "&hql=" + hql);
        return "all-tour-list";
    }

    @RequestMapping("list-tour")
    public String listTour(@RequestParam("page") int page, ModelMap m) {
        int limit = 4;
        List<Tour> lstt = pagination.getByPage(limit, page, tDAO.getAll());
        m.addAttribute("pagenumber", pagination.pageNumber(limit, tDAO.getAll()));
        m.addAttribute("lstt", lstt);
        m.addAttribute("page", page);
        m.addAttribute("lstkv", kvDAO.getAll());
        m.addAttribute("lstth", thDAO.getAll());
        return "all-tour-list";
    }

    @RequestMapping("sort")
    public String lowestPrice(@RequestParam("page") int page, @RequestParam("sort") String sort, ModelMap m) {
        String hql = null;
        if (sort.equals("lower") == true) {
            hql = "FROM Tour ORDER BY dongia";
        } else if (sort.equals("higher") == true) {
            hql = "FROM Tour ORDER BY dongia DESC";
        }
        int limit = 4;
        List<Tour> lstt = pagination.getByPage(limit, page, tDAO.getByHql(hql));
        m.addAttribute("pagenumber", pagination.pageNumber(limit, tDAO.getByHql(hql)));
        m.addAttribute("lstt", lstt);
        m.addAttribute("page", page);
        m.addAttribute("lstkv", kvDAO.getAll());
        m.addAttribute("lstth", thDAO.getAll());
        return "all-tour-list";

    }

    @RequestMapping("filter")
    public String filter(@RequestParam(required = false, value = "thuonghieu") String[] thuonghieu,
            @RequestParam(required = false, value = "khuvuc") String[] khuvuc, @RequestParam("page") int page,
            ModelMap m) {

        // String hql="FROM Tour WHERE thuonghieu.tenthuonghieu = 'Rồng Á Châu'";
        String hql = "FROM Tour WHERE ";
        if (thuonghieu != null && thuonghieu.length > 0) {
            for (int i = 0; i < thuonghieu.length; i++) {
                String str = "thuonghieu.tenthuonghieu='" + thuonghieu[i] + "'";
                hql = hql.concat(str);
                if (i < thuonghieu.length - 1) {
                    hql = hql.concat(" OR ");
                }
            }
            if (khuvuc != null) {
                hql = hql.concat(" AND ");
            }
        }
        if (khuvuc != null && khuvuc.length > 0) {
            for (int i = 0; i < khuvuc.length; i++) {
                String str = "khuvuc.tenkhuvuc='" + khuvuc[i] + "'";
                hql = hql.concat(str);
                if (i < khuvuc.length - 1) {
                    hql = hql.concat(" OR ");
                }
            }
        }
        int limit = 4;
        List<Tour> lstt = pagination.getByPage(limit, page, tDAO.getByHql(hql));
        m.addAttribute("pagenumber", pagination.pageNumber(limit, tDAO.getByHql(hql)));
        m.addAttribute("lstt", lstt);
        m.addAttribute("page", page);
        m.addAttribute("lstkv", kvDAO.getAll());
        m.addAttribute("lstth", thDAO.getAll());
        return "all-tour-list";
    }
    @RequestMapping("singletour")
    public String singleTour(@RequestParam("id") int id,ModelMap m){
        Tour tour = tDAO.getByid(id);
        m.addAttribute("tour",tour);
         String hql="FROM Hinhanhchitiet WHERE tour.id="+id;
         m.addAttribute("lstha",haDAO.getByHql(hql));
            m.addAttribute("id",id);
        return"single-tour";
    }
    
}
