/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.booking.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import travel.admin.DAO.TourDAO;
import travel.entity.Tour;
import travel.utility.Cart;
import travel.utility.CartItem;

/**
 *
 * @author Phuoc
 */
@Controller("CartBooking")
@Transactional
@RequestMapping("booking/cart")
public class CartController {

    @Autowired
    Cart cart;
    @Autowired
    TourDAO tDAO;

    @RequestMapping("add")
    public String add(@RequestParam("id") int id,
            @RequestParam("q1") int slNguoilon,
            @RequestParam("q2") int slTreem, ModelMap m) {
        CartItem cartItem = new CartItem();
        cartItem.setSlNguoilon(slNguoilon);
        cartItem.setSlTreem(slTreem);
        cartItem.setTour(tDAO.getByid(id));
        cart.addItem(id, cartItem);
        List<CartItem> listItem = new ArrayList<CartItem>();
        Collection<CartItem> list = cart.getItems();
        int slnl = 0;
        int slte = 0;
        for (CartItem item : list) {
            listItem.add(item);
            slnl += item.getSlNguoilon();
            slte += item.getSlTreem();
        }
        m.addAttribute("listItem", listItem);
        m.addAttribute("sum", cart.sum());
        m.addAttribute("slnl", slnl);
        m.addAttribute("slte", slte);
        m.addAttribute("total", cart.total());
        m.addAttribute("listItem", listItem);
        return "cart";
    }

    @RequestMapping("action")
    public String delete(@RequestParam("id") int id,
            @RequestParam(required = false, value = "action") String action,
            @RequestParam("q1") int slNguoilon,
            @RequestParam("q2") int slTreem, ModelMap m) {
        if (action.equals("update")) {
            cart.updateQuantity(id, slNguoilon, slTreem);
        } else if (action.equals("delete")) {
            cart.removeItem(id);
        }
        List<CartItem> listItem = new ArrayList<CartItem>();
        int slnl = 0;
        int slte = 0;
        Collection<CartItem> list = cart.getItems();
        for (CartItem item : list) {
            listItem.add(item);
            slnl += item.getSlNguoilon();
            slte += item.getSlTreem();
        }
        m.addAttribute("listItem", listItem);
        m.addAttribute("sum", cart.sum());
        m.addAttribute("slnl", slnl);
        m.addAttribute("slte", slte);
        m.addAttribute("total", cart.total());
        return "cart";
    }

}
