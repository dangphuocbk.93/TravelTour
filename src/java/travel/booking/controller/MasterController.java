/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.booking.controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import travel.admin.DAO.NguoidungDAO;
import travel.entity.Nguoidung;

/**
 *
 * @author Phuoc
 */
@Controller("MasterBooking")
@RequestMapping(value = "booking/master")
public class MasterController {

    @Autowired
    NguoidungDAO ndDAO;

    @RequestMapping("signup")
    String signup() {
        return "login/login";
    }

    @RequestMapping("register")
    String register(ModelMap m) {
        m.addAttribute("nd", new Nguoidung());
        return "login/register";
    }

    @RequestMapping(value = "register_finish", method = RequestMethod.POST)
    public String register_finish(@ModelAttribute("nd") Nguoidung nd, ModelMap m) {
        List<Nguoidung> lstnd = ndDAO.getAll();
        for (Nguoidung nd2 : lstnd) {
            if (nd.getEmail().equals(nd2.getEmail())) {
                m.addAttribute("message", "Tài khoản đã tồn tại! ");
                return "redirect:/booking/master/register.htm";
            }
        }
        ndDAO.insert(nd);

        return "redirect:/booking/master/signup.htm";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(@RequestParam("email") String email, @RequestParam("password") String password, ModelMap m, HttpSession httpSession) {
        List<Nguoidung> lstnd = ndDAO.getAll();
        Nguoidung nd = null;
        String message = null;
        for (Nguoidung nd2 : lstnd) {
            if (email.equals(nd2.getEmail())) {
                nd = nd2;
                break;
            }
        }
        if (nd == null) {
            message = "Tài khoản không tồn tại!";
        } else if (!password.equals(nd.getPassword())) {
            message = "Sai mật khẩu!";
        } else {
            httpSession.setAttribute("nd", nd);
            m.addAttribute("nd",nd);
            return "redirect:/booking/tour/newtour.htm";

        }
        m.addAttribute("message", message);
        return "login/login";
    }

    @RequestMapping("logout")
    public String signout(ModelMap m, HttpSession httpSession) {
        httpSession.removeAttribute("nd");
        m.addAttribute("message", "Đã đăng xuất!");
        return "redirect:/booking/tour/newtour.htm";
    }
}
