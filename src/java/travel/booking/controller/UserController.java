/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.booking.controller;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import travel.admin.DAO.NguoidungDAO;
import travel.entity.Nguoidung;

/**
 *
 * @author Phuoc
 */
@Controller("UserBooking")
@Transactional
@RequestMapping("booking/user")
public class UserController {
   @Autowired
   NguoidungDAO ndDAO;
       @Autowired
    ServletContext context;        
   @RequestMapping("profile")
    public String profile(){
       return "profile";
   } 
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(@RequestParam("path")MultipartFile file,@ModelAttribute("nd") Nguoidung nd, ModelMap m, HttpSession httpSession) throws IOException{          
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = context.getRealPath("/");
            // Đường dẫn đến thư mục gốc
            String pathNew = path.substring(0, path.length() - 11);
            File fileSaveDir = new File(pathNew + "/web/sources/booking/img/nguoidung/"+nd.getEmail());
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdirs();
            }
            String uploadFilePath = fileSaveDir.getAbsolutePath() + File.separator + fileName;
            file.transferTo(new File(uploadFilePath));
        }
        nd.setHinhanh(file.getOriginalFilename());
        ndDAO.update(nd);
        m.addAttribute("nd",nd);
        httpSession.setAttribute("nd",nd);
       return "redirect:/booking/user/profile.htm";

    }
}
