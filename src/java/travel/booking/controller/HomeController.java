/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.booking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Phuoc
 */
@Controller("HomeBooking")
@RequestMapping(value = "booking")
public class HomeController {

    @RequestMapping("home")
    public String index() {
        return "main-content";
    }
}
