package travel.utility;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Component;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
@Component
public class HttpContext{
	/**
	 * Bộ lọc được sử dụng để gắng kết response lên request tương ứng
	 */
	public static class Interceptor extends HandlerInterceptorAdapter{
		@Override
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
			HttpContext.getRequest().setAttribute("response", response);
			return true;
		}
	}
	
	/**
	 * Lấy request hiện tại
	 */
	public static HttpServletRequest getRequest() {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attributes.getRequest();
	}
	
	/**
	 * Lấy response tương ứng với request hiện tại
	 */
	public static HttpServletResponse getResponse() {
		return (HttpServletResponse) getRequest().getAttribute("response");
	}
	
	/**
	 * Lấy session hiện tại
	 */
	public static HttpSession getSession() {
		return getRequest().getSession();
	}
	
	/**
	 * Lấy servlet context của ứng dụng web
	 */
	public static ServletContext getServletContext() {
		return getRequest().getServletContext();
	}
	
	/**
	 * Lấy đường vật lý
	 * @param folder là đường dẫn ảo trong website
	 * @return đường dẫn vật lý của đường dẫn ảo
	 */
	public static String getRealPath(String folder) {
		return getServletContext().getRealPath(folder);
	}
	
	/**
	 * Lấy địa chỉ url tuyệt đối hiện tại
	 */
	public static String getUrl() {
		return getRequest().getRequestURL().toString();
	}
	
	/**
	 * Lấy địa chỉ url tương đối hiện tại
	 */
	public static String getUri() {
		return getRequest().getRequestURI();
	}
}
