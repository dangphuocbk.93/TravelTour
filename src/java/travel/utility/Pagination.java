/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.utility;

import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Phuoc
 */

@Component
@Transactional
public class Pagination {
    public List getByPage(int limit, int selectPage, List lst){
        int start =0;       
        if(selectPage>1){
            start=(selectPage-1)*limit;
        }
        else if(selectPage==1){
            start=0;
        }
        List lst1;
        if(lst.size()<(limit*selectPage)){
           lst1=lst.subList(start,lst.size());
        }
        else{
            lst1=lst.subList(start,(start+limit));
        }
        return lst1 ;
   }
    // Tính số trang
    public int pageNumber(int limit, List lst){
        int pageNumber;
        int tong = lst.size();
        if(tong%limit==0){
            pageNumber=tong/limit;
        }
        else{
            pageNumber=Math.round(tong/limit)+1;
        }
        return pageNumber;
    }
}
