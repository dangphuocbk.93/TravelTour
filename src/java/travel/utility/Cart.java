/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.utility;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Phuoc
 */
@Component
@Transactional
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value="session")
public class Cart implements Serializable{
    // Biến thành viên
    Map<Integer,CartItem> cartItems= new HashMap<Integer, CartItem>() ;
            
    // Phương thức thêm item vào giỏ hàng
    public void addItem(int id, CartItem item) {
        if(cartItems.containsKey(id)!=true){
            cartItems.put(id,item);
        }  
    }
     // Phương thức xóa tour trong giỏ hàng
     public void removeItem (int id){
        if(cartItems.containsKey(id)==true){
            cartItems.remove(id);
        }    
     }
     // Phương thức xóa sạch các tour trong giỏ hàng
     public void removeAll(){
         cartItems.clear();
     }
     // Phương thức update số lượng tour
     public void updateQuantity(int id, int slNguoilon,int slTreem){
         if(cartItems.containsKey(id)){
             cartItems.get(id).setSlNguoilon(slNguoilon);
             cartItems.get(id).setSlTreem(slTreem);
         }
     }
    // Phương thức tính tổng tour có trong giỏ hàng
    public int sum(){
        Collection<CartItem> lstItem = cartItems.values();
        return lstItem.size();
    }
    // Phương thức tính tổng giá trị 
     public double total(){
         double sum = 0;
         Collection<CartItem> lstItem = cartItems.values();
         for(CartItem item : lstItem){
             sum+= (item.getSlNguoilon()*item.getTour().getDongiaKm())+ (0.5)*(item.getSlTreem()*item.getTour().getDongiaKm());
         }
         return sum;
     }
     //Phương thức tính giá trị của 1 item
     public double price(int id){
         CartItem item = cartItems.get(id);
         double price = (item.getSlNguoilon()*item.getTour().getDongiaKm())+ (0.5)*(item.getSlTreem()*item.getTour().getDongiaKm());
         return price;
     }
     // Phương thức lấy tập hợp các item
     public Collection<CartItem> getItems(){       
         return cartItems.values();
     }
}
