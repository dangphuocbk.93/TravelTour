/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.utility;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import travel.entity.Tour;

/**
 *
 * @author Phuoc
 */
@Component
@Transactional
public class CartItem {
    // Biến thành viên
    Tour tour;
    int slNguoilon;
    int slTreem;
    // Thuộc tính
    public Tour getTour() {
        return tour;
    }

    public int getSlNguoilon() {
        return slNguoilon;
    }

    public int getSlTreem() {
        return slTreem;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public void setSlNguoilon(int slNguoilon) {
        this.slNguoilon = slNguoilon;
    }

    public void setSlTreem(int slTreem) {
        this.slTreem = slTreem;
    }  
    // Phương thức khởi tạo không tham số

    public CartItem() {
    }
    // Phương thức khởi tạo có tham sô

    public CartItem(Tour tour, int slNguoilon, int slTreem) {
        this.tour = tour;
        this.slNguoilon = slNguoilon;
        this.slTreem = slTreem;
    }

 
    
}
