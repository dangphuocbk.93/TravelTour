/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.utility;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Phuoc
 */

@Component
@Transactional
public class Pagination1 {
    @Autowired
    SessionFactory sessionFactory;
    public List getByPage(int limit, int selectPage, String hql){
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        int start =0;       
        if(selectPage>1){
            start=(selectPage-1)*limit;
        }
        else if(selectPage==1){
            start=0;
        }
        query.setFirstResult(start);
        query.setMaxResults(limit);
        
        return query.list();
   }
    // Tính số trang
    public int pageNumber(int limit, String hql){
         Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        int pageNumber;
        int tong = query.list().size();
        if(tong%limit==0){
            pageNumber=tong/limit;
        }
        else{
            pageNumber=Math.round(tong/limit)+1;
        }
        return pageNumber;
    }
}
