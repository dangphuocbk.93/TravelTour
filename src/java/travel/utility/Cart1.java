/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.utility;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Phuoc
 */
@Component
@Transactional
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value="session")
public class Cart1 implements Serializable{
    // Biến thành viên
    HashMap<Integer, CartItem> cartItems;
    // Thuộc tính
    public HashMap<Integer, CartItem> getCartItems() {
        return cartItems;
    }
    public void setCartItems(HashMap<Integer, CartItem> cartItems) {
        this.cartItems = cartItems;
    }
    // Phương thức khai báo không tham sô

    public Cart1() {
    }
    // Phương thức khai báo cáo có tham số
    // Biến cục bộ  
    public Cart1(HashMap<Integer, CartItem> cartItems) {
        this.cartItems = cartItems;
    }
    // Phương thức thêm tour vào giỏ hàng
    public void addItem(Integer id, CartItem item){        
           if(cartItems.containsKey(id)!=true){
             cartItems.put(id,item);
        }  
    }
     // Phương thức xóa tour trong giỏ hàng
     public void removeItem (int id){
        if(cartItems.containsKey(id)==true){
            cartItems.remove(id);
        }    
     }
     // Phương thức xóa sạch các tour trong giỏ hàng
     public void removeAll(){
         cartItems.clear();
     }
     // Phương thức update số lượng tour
     public void updateQuantity(int id, int slNguoilon,int slTreem){
         if(cartItems.containsKey(id)){
             cartItems.get(id).setSlNguoilon(slNguoilon);
             cartItems.get(id).setSlTreem(slTreem);
         }
     }
    // Phương thức tính tổng tour có trong giỏ hàng
    public int tongSP(){
        Collection<CartItem> lstItem = cartItems.values();
        return lstItem.size();
    }
    // Phương thức tính tổng giá trị 
     public double tongGiaTri(){
         double sum = 0;
         Collection<CartItem> lstItem = cartItems.values();
         for(CartItem item : lstItem){
             sum+= (item.getSlNguoilon()*item.getTour().getDongiaKm())+ (0.5)*(item.getSlTreem()*item.getTour().getDongiaKm());
         }
         return sum;
     }
     // Phương thức lấy tập hợp các item
     public Collection<CartItem> getItems(){       
         return cartItems.values();
     }
}
