/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.DAO;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import travel.entity.Hinhanhchitiet;

/**
 *
 * @author Phuoc
 */
@Repository 
@Transactional
public class HinhanhDAO {
    @Autowired
    SessionFactory sessionFactory;
// Truy vấn theo hql
public List<Hinhanhchitiet> getByHql(String hql){
       Session session = sessionFactory.getCurrentSession();
       Query query = session.createQuery(hql);
       List<Hinhanhchitiet> lstimg = query.list();
       return lstimg;
    }    
// Truy vấn toàn bộ
    public List<Hinhanhchitiet> getAll(){      
        Session session = sessionFactory.getCurrentSession();      
        Query query = session.createQuery("FROM Hinhanhchitiet");
        List<Hinhanhchitiet> lstimg = query.list();
        return lstimg;
    }
    // Truy vấn theo id
    public Hinhanhchitiet getByid(int id){
        Session session = sessionFactory.getCurrentSession();
        Hinhanhchitiet img = (Hinhanhchitiet)session.get(Hinhanhchitiet.class, id);
        return img;
    }
    //Insert đối tượng mới
    public void insert(Hinhanhchitiet img){
        Session session = sessionFactory.getCurrentSession();
        session.save(img);
    }
    // Delete đối tượng
    public void delete(int id){
        Session session = sessionFactory.getCurrentSession();
        Hinhanhchitiet img = (Hinhanhchitiet)session.get(Hinhanhchitiet.class, id);
        session.delete(img);
    }
    // Cập nhật đối tượng
    public void update(Hinhanhchitiet img){
        Session session = sessionFactory.getCurrentSession();
        session.update(img);
    }
}
