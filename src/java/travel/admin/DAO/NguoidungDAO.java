/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.DAO;

import java.util.*;
import org.hibernate.*;
import travel.entity.Nguoidung;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Phuoc
 */
@Repository
@Transactional
public class NguoidungDAO {

    @Autowired
    SessionFactory sessionFactory;    
    // Truy vấn toàn bộ
    public List<Nguoidung> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Nguoidung");
        List<Nguoidung> lstnd = query.list();
        return lstnd;
    }

    // Truy vấn theo id
    public Nguoidung getByid(int id) {
        Session session = sessionFactory.getCurrentSession();
        Nguoidung nd = (Nguoidung) session.get(Nguoidung.class, id);
        return nd;
    }

    //Insert đối tượng mới
    public void insert(Nguoidung nd) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(nd);
    }

    // Delete đối tượng
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Nguoidung nd = (Nguoidung) session.get(Nguoidung.class, id);
        session.delete(nd);
    }

    // Cập nhật đối tượng
    public void update(Nguoidung nd) {
        Session session = sessionFactory.getCurrentSession();
        session.update(nd);
    }

}
