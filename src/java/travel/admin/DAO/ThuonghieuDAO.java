/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.DAO;

import java.util.*;
import org.hibernate.*;
import travel.entity.Thuonghieu;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Phuoc
 */
@Repository
@Transactional
public class ThuonghieuDAO {
     @Autowired
    SessionFactory sessionFactory;
    // Truy vấn theo hql
    public List<Thuonghieu> getByHql(String hql){
       Session session = sessionFactory.getCurrentSession();
       Query query = session.createQuery(hql);
       List<Thuonghieu> lstth = query.list();
       return lstth;
    }
    // Truy vấn toàn bộ
    public List<Thuonghieu> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Thuonghieu");
        List<Thuonghieu> lstth = query.list();
        return lstth;
    }

    // Truy vấn theo id
    public Thuonghieu getByid(int id) {
        Session session = sessionFactory.getCurrentSession();
        Thuonghieu th = (Thuonghieu) session.get(Thuonghieu.class, id);
        return th;
    }

    //Insert đối tượng mới
    public void insert(Thuonghieu th) {
        Session session = sessionFactory.getCurrentSession();
        session.save(th);
    }

    // Delete đối tượng
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Thuonghieu th = (Thuonghieu) session.get(Thuonghieu.class, id);
        session.delete(th);
    }

    // Cập nhật đối tượng
    public void update(Thuonghieu th) {
        Session session = sessionFactory.getCurrentSession();
        session.update(th);
    }
}
