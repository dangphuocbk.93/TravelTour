/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.DAO;

import java.util.*;
import org.hibernate.*;
import travel.entity.Tour;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Phuoc
 */
@Repository
@Transactional
public class TourDAO {

    @Autowired
    SessionFactory sessionFactory;
    // Truy vấn theo hql
    public List<Tour> getByHql(String hql){
       Session session = sessionFactory.getCurrentSession();
       Query query = session.createQuery(hql);
       List<Tour> lstt = query.list();
       return lstt;
    }
    // Truy vấn theo sql
    public List<Tour> getBySql(String sql){
       Session session = sessionFactory.getCurrentSession();
       SQLQuery query = session.createSQLQuery(sql);
       query.addEntity(Tour.class);
       List<Tour> lstt = query.list();
       return lstt;
    }
    // Truy vấn toàn bộ
    public List<Tour> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Tour");
        List<Tour> lstt = query.list();
        return lstt;
    }

    // Truy vấn theo id
    public Tour getByid(int id) {
        Session session = sessionFactory.getCurrentSession();
        Tour t = (Tour) session.get(Tour.class, id);
        return t;
    }

    //Insert đối tượng mới
    public void insert(Tour t) {
        Session session = sessionFactory.getCurrentSession();
        session.save(t);
    }

    // Delete đối tượng
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Tour t = (Tour)session.get(Tour.class, id);
        session.delete(t);
    }

    // Cập nhật đối tượng
    public void update(Tour t) {
        Session session = sessionFactory.getCurrentSession();
        session.update(t);
    }


}
