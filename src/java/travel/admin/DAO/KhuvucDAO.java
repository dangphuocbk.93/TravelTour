/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.DAO;

import java.util.*;
import org.hibernate.*;
import travel.entity.Khuvuc;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Phuoc
 */
@Repository 
@Transactional
public class KhuvucDAO {
    @Autowired
    SessionFactory sessionFactory;
    // Truy vấn theo hql
    public List<Khuvuc> getByHql(String hql){
       Session session = sessionFactory.getCurrentSession();
       Query query = session.createQuery(hql);
       List<Khuvuc> lstkv = query.list();
       return lstkv;
    }    
// Truy vấn toàn bộ
    public List<Khuvuc> getAll(){      
        Session session = sessionFactory.getCurrentSession();      
        Query query = session.createQuery("FROM Khuvuc");
        List<Khuvuc> lstkv = query.list();
        return lstkv;
    }
    // Truy vấn theo id
    public Khuvuc getByid(int id){
        Session session = sessionFactory.getCurrentSession();
        Khuvuc kv = (Khuvuc)session.get(Khuvuc.class, id);
        return kv;
    }
    //Insert đối tượng mới
    public void insert(Khuvuc kv){
        Session session = sessionFactory.getCurrentSession();
        session.save(kv);
    }
    // Delete đối tượng
    public void delete(int id){
        Session session = sessionFactory.getCurrentSession();
        Khuvuc kv = (Khuvuc)session.get(Khuvuc.class, id);
        session.delete(kv);
    }
    // Cập nhật đối tượng
    public void update(Khuvuc kv){
        Session session = sessionFactory.getCurrentSession();
        session.update(kv);
    }
}
