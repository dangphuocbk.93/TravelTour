/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import travel.admin.DAO.HinhanhDAO;
import travel.admin.DAO.TourDAO;
import travel.entity.Hinhanhchitiet;
import travel.utility.Pagination;

/**
 *
 * @author Phuoc
 */
@Controller
@Transactional
@RequestMapping("admin/img")
public class ImageController {

    @Autowired
    HinhanhDAO haDAO;
     @Autowired
    TourDAO tDAO;
    @Autowired
    ServletContext context;
    @Autowired
    Pagination pagination;

    @RequestMapping("getall")
    public String getAll(@RequestParam("id_tour")int id_tour,@RequestParam("page")int page, ModelMap m){
        int limit=4;
        String hql="FROM Hinhanhchitiet WHERE tour.id="+id_tour;       
        List<Hinhanhchitiet> lstha = pagination.getByPage(limit, page, haDAO.getByHql(hql));      
        m.addAttribute("lstha",lstha);
        m.addAttribute("pagenumber",pagination.pageNumber(limit, haDAO.getByHql(hql)));
        m.addAttribute("ha", new Hinhanhchitiet()); 
        m.addAttribute("id_tour",id_tour);
        m.addAttribute("page",page);
        return "admin/image-detail";
    }
    
    @RequestMapping("add")
    public String Add(@RequestParam("path") MultipartFile file,@RequestParam("page") int page,@ModelAttribute("ha") Hinhanhchitiet ha ,ModelMap m) throws IOException {
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = context.getRealPath("/");
            // Đường dẫn đến thư mục gốc
            String pathNew = path.substring(0, path.length() - 11);
            File fileSaveDir = new File(pathNew + "/web/sources/admin/dist/img/tour/"+ha.getTour().getId());
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdirs();
            }
            String uploadFilePath = fileSaveDir.getAbsolutePath() + File.separator + fileName;
            file.transferTo(new File(uploadFilePath));
        }      
        ha.setHinhanh(file.getOriginalFilename());
        List<Hinhanhchitiet> lstha = haDAO.getAll();
        int dem=0;
        for(Hinhanhchitiet ha1 : lstha){
            if(ha1.getHinhanh().equals(file.getOriginalFilename())){
               dem=dem+1; 
            }
        }
        if(dem==0){
             haDAO.insert(ha);
        }
        m.addAttribute("id_tour",ha.getTour().getId());
        m.addAttribute("page",page);
        return "redirect:/admin/img/getall.htm";
    }
    
    @RequestMapping("delete")
    public String delete(@RequestParam("id_ha")int id_ha,@RequestParam("page") int page, ModelMap m){      
        Hinhanhchitiet ha = haDAO.getByid(id_ha);
        m.addAttribute("id_tour", ha.getTour().getId());
        m.addAttribute("page",page);
        haDAO.delete(id_ha);
       return "redirect:/admin/img/getall.htm";
    }
}
