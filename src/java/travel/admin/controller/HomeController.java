/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import travel.entity.*;

/**
 *
 * @author Phuoc
 */
@Controller
@RequestMapping("admin")
public class HomeController {
   @RequestMapping("login")
   public String index(ModelMap m){
       m.put("nd", new Nguoidung());
       return "admin/login";
   }
   @RequestMapping("home")
   public String index(){
      
       return "admin/home";
   }
}
