/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.controller;
import java.io.File; 
import javax.servlet.ServletContext; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Controller; 
import org.springframework.ui.ModelMap; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.multipart.MultipartFile; 
/**
 *
 * @author Phuoc
 */

 
@Controller
@RequestMapping("admin/uploadfile")
public class UpLoadFileController { 
  @Autowired 
  ServletContext context; 
 
  /* 
   * GET: single.htm 
   */ 
  @RequestMapping(value="/single", method=RequestMethod.GET) 
  public String showUploadFileForm() { 
    return "single"; 
  } 
  @RequestMapping(value="upload", method=RequestMethod.POST) 
  public String submitUploadFileForm(@RequestParam("path")MultipartFile file, ModelMap model) throws Exception { 
    if(!file.isEmpty()){ 
      String fileName = file.getOriginalFilename(); 
      long fileSize = file.getSize(); 
      String path =context.getRealPath("/"); 
      // Đường dẫn đến thư mục gốc D:\J2EE\WebBook
       String pathNew = path.substring(0,path.length() - 11);
       String uploadFilePath = pathNew + "/web/sources/admin/dist/img/"+fileName;
        File fileSaveDir = new File(pathNew +"/web/sources/admin/dist/img/");          
            if (!fileSaveDir.exists()){
                fileSaveDir.mkdirs();               
            }
        String pathEnd = pathNew + "/web/sources/admin/dist/img/"+fileName;
      file.transferTo(new File(uploadFilePath));
      
      model.addAttribute("filename", fileName); 
      model.addAttribute("filesize", fileSize); 
      model.addAttribute("a","Upload file Directory = " + fileSaveDir.getAbsolutePath()); 
      model.addAttribute("path", uploadFilePath); 
    } 
    return "admin/vd"; 
  } 
}