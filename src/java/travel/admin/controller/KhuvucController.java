/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.controller;

import travel.admin.DAO.KhuvucDAO;
import java.util.*;
import travel.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import travel.utility.Pagination;

/**
 *
 * @author Phuoc
 */
@Controller
@Transactional
@RequestMapping("admin/khuvuc")
public class KhuvucController {

    @Autowired
    KhuvucDAO kvDAO;
    @Autowired
    Pagination pagination;

    @RequestMapping(value = "getall")
    public String getAll(@RequestParam("page") int page, ModelMap m) {
        int limit = 4;
        List<Khuvuc> lstkv = pagination.getByPage(limit, page, kvDAO.getAll());
        m.addAttribute("pagenumber", pagination.pageNumber(limit, kvDAO.getAll()));
        m.addAttribute("page", page);
        m.addAttribute("lstkhuvuc", lstkv);
        m.addAttribute("kv", new Khuvuc());
        return "admin/allkhuvuc";
    }

    @RequestMapping(value = "delete")
    public String delete(@RequestParam("page") int page, @RequestParam("id_kv") int id_kv, ModelMap m) {
        kvDAO.delete(id_kv);
        m.addAttribute("page", page);
        return "redirect:/admin/khuvuc/getall.htm";
    }

    @RequestMapping(value = "update")
    public String update(@RequestParam("page") int page, @ModelAttribute("kv") Khuvuc kv, ModelMap m) {
        kvDAO.update(kv);
        m.addAttribute("page", page);
        return "redirect:/admin/khuvuc/getall.htm";
    }

    @RequestMapping(value = "insert")
    public String insert(@RequestParam("page") int page, @ModelAttribute("kv") Khuvuc kv, ModelMap m) {
        kvDAO.insert(kv);
        m.addAttribute("page", page);
        return "redirect:/admin/khuvuc/getall.htm";
    }

}
