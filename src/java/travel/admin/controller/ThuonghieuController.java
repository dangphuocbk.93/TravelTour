/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import travel.admin.DAO.ThuonghieuDAO;
import travel.entity.Thuonghieu;
import travel.utility.Pagination;

/**
 *
 * @author Phuoc
 */
@Controller
@Transactional
@RequestMapping("admin/thuonghieu")
public class ThuonghieuController {

    @Autowired
    ThuonghieuDAO thDAO;
    @Autowired
    Pagination pagination;
    @Autowired
    ServletContext context;

    @RequestMapping(value = "getall")
    public String getAll(@RequestParam("page") int page, ModelMap m) {
        int limit = 4;
        List<Thuonghieu> lstth = pagination.getByPage(limit, page, thDAO.getAll());
        m.addAttribute("lstthuonghieu", lstth);
        m.addAttribute("pagenumber", pagination.pageNumber(limit, thDAO.getAll()));
        m.addAttribute("page", page);
        m.addAttribute("th", new Thuonghieu());
        return "admin/allthuonghieu";
    }

    @RequestMapping(value = "delete")
    public String delete(@RequestParam("page") int page, @RequestParam("id_th") int id_th, ModelMap m) {
        thDAO.delete(id_th);
        m.addAttribute("page", page);
        return "redirect:/admin/thuonghieu/getall.htm";
    }

    @RequestMapping(value = "update")
    public String update(@RequestParam("page") int page, @RequestParam("path") MultipartFile file, @ModelAttribute("th") Thuonghieu th, ModelMap m) throws IOException {
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = context.getRealPath("/");
            // Đường dẫn đến thư mục gốc
            String pathNew = path.substring(0, path.length() - 11);
            File fileSaveDir = new File(pathNew + "/web/sources/admin/dist/img/thuonghieu/" + th.getId());
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdirs();
            }
            String uploadFilePath = fileSaveDir.getAbsolutePath() + File.separator + fileName;
            file.transferTo(new File(uploadFilePath));
            th.setHinhanh(file.getOriginalFilename());
        }
        thDAO.update(th);
        m.addAttribute("page", page);
        return "redirect:/admin/thuonghieu/getall.htm";
    }

    @RequestMapping(value = "insert")
    public String insert(@RequestParam("page") int page, @RequestParam("path") MultipartFile file, @ModelAttribute("th") Thuonghieu th, ModelMap m) throws IOException {
        th.setHinhanh(file.getOriginalFilename());
        thDAO.insert(th);
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = context.getRealPath("/");
            // Đường dẫn đến thư mục gốc
            String pathNew = path.substring(0, path.length() - 11);
            File fileSaveDir = new File(pathNew + "/web/sources/admin/dist/img/thuonghieu/" + th.getId());
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdirs();
            }
            String uploadFilePath = fileSaveDir.getAbsolutePath() + File.separator + fileName;
            file.transferTo(new File(uploadFilePath));
        }
        m.addAttribute("page", page);
        return "redirect:/admin/thuonghieu/getall.htm";
    }
}
