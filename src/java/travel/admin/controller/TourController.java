package travel.admin.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import travel.entity.*;
import travel.admin.DAO.KhuvucDAO;
import travel.admin.DAO.ThuonghieuDAO;
import travel.admin.DAO.TourDAO;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import travel.admin.DAO.HinhanhDAO;
import travel.utility.Pagination;

@Controller("admin")
@Transactional
@RequestMapping("admin/tour")
public class TourController {

    @Autowired
    TourDAO tDAO;
    @Autowired
    KhuvucDAO kvDAO;
    @Autowired
    ThuonghieuDAO thDAO;
    @Autowired
    HinhanhDAO haDAO;
    @Autowired
    ServletContext context;
    @Autowired
    Pagination pagination;

    @RequestMapping("singletour")
    public String singletour() {
        return "admin/singletour";
    }

    @RequestMapping("getall")
    public String getAll(@RequestParam("page") int page, ModelMap m) {
        int limit = 4;
        List<Tour> lsttour = pagination.getByPage(limit, page, tDAO.getAll());
        m.addAttribute("pagenumber", pagination.pageNumber(limit, tDAO.getAll()));
        m.addAttribute("page", page);
        m.addAttribute("lsttour", lsttour);
        return "admin/alltour";
    }
    // Truy xuất tour và hiển thị thông tin

    @RequestMapping("edit")
    public String getById(@RequestParam("id_tour") int id_tour, ModelMap m) {
        m.addAttribute("tour", tDAO.getByid(id_tour));
        m.addAttribute("lstkv", kvDAO.getAll());
        m.addAttribute("lstth", thDAO.getAll());
        m.addAttribute("ha", new Hinhanhchitiet());
        return "admin/singletour";
    }
    // Delete tour

    @RequestMapping(value = "delete")
    public String delete(@RequestParam("id_tour") int id_tour, @RequestParam("page") int page, ModelMap m) {
        tDAO.delete(id_tour);
        m.addAttribute("page", page);
        return "redirect:/admin/tour/getall.htm";
    }

    // Cập nhật tour
    @RequestMapping("update")
    public String update(@RequestParam("path") MultipartFile file, @ModelAttribute("tour") Tour tour, ModelMap m) throws IOException {
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = context.getRealPath("/");
            // Đường dẫn đến thư mục gốc
            String pathNew = path.substring(0, path.length() - 11);
            File fileSaveDir = new File(pathNew + "/web/sources/admin/dist/img/tour/" + tour.getId());
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdirs();
            }
            String uploadFilePath = fileSaveDir.getAbsolutePath() + File.separator + fileName;
            file.transferTo(new File(uploadFilePath));
            tour.setHinhanh(file.getOriginalFilename());
        }
        tDAO.update(tour);
        m.addAttribute("message", "Cập nhật thành công!");
        m.addAttribute("id_tour", tour.getId());
        //return "admin/singletour";
        return "redirect:/admin/tour/edit.htm";

    }

    @RequestMapping("add")
    public String add(ModelMap m) {
        m.addAttribute("tour", new Tour());
        m.addAttribute("lstkv", kvDAO.getAll());
        m.addAttribute("lstth", thDAO.getAll());
        return "admin/insert-tour";
    }

    @RequestMapping("insert")
    public String insert(@RequestParam("path") MultipartFile file, @ModelAttribute("tour") Tour tour, ModelMap m) throws IOException {
        tour.setHinhanh(file.getOriginalFilename());
        tDAO.insert(tour);
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();
            String path = context.getRealPath("/");
            // Đường dẫn đến thư mục gốc
            String pathNew = path.substring(0, path.length() - 11);
            File fileSaveDir = new File(pathNew + "/web/sources/admin/dist/img/tour/" + tour.getId());
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdirs();
            }
            String uploadFilePath = fileSaveDir.getAbsolutePath() + File.separator + fileName;
            file.transferTo(new File(uploadFilePath));
        }
        m.addAttribute("message", "Thêm mới thành công!");
        m.addAttribute("id_tour", tour.getId());
        //return "admin/singletour";        
        return "redirect:/admin/tour/edit.htm";
    }
}
