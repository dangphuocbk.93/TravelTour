/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.admin.controller;

import travel.admin.DAO.NguoidungDAO;
import java.util.*;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import travel.entity.*;

/**
 *
 * @author Phuoc
 */
@Controller
@Transactional
@RequestMapping(value = "admin/master")
public class MasterController {

    @Autowired
    NguoidungDAO ndDAO;
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(@ModelAttribute(value = "nd") Nguoidung tk, ModelMap m, HttpSession httpSession) {
        List<Nguoidung> lstnd = ndDAO.getAll();
        Nguoidung nd = null;
        String message = null;
        for (Nguoidung nd2 : lstnd) {
            if (tk.getEmail().equals(nd2.getEmail())) {
                nd = nd2;
                break;
            }
        }
        if (nd == null) {
            message = "Tài khoản không tồn tại!";
        } else if (!tk.getPassword().equals(nd.getPassword())) {
            message = "Sai mật khẩu!";
        } else if(tk.getPassword().equals(nd.getPassword()) && nd.getVaitro().getId()==1){
            message = "Tài khoản không được sử dụng cho Admin!";
        }
        else {
            httpSession.setAttribute("nd", nd);
            return "redirect:/admin/home.htm";
        }

        m.addAttribute("message", message);
        return "admin/login";
    }
   @RequestMapping("signout")
        public String signout(ModelMap m, HttpSession httpSession){
        httpSession.removeAttribute("nd");
        m.addAttribute("message","Đã đăng xuất!");
            return "admin/login";
   }

}
