<%-- 
    Document   : single-tour
    Created on : May 10, 2017, 8:38:37 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container margin_60">
    <div class="row">
        <div class="col-md-8" id="single_tour_desc">
            <!-- Tour-feat -->
            <jsp:include page="single-tour/tour-feat.jsp"/>
            <!-- img-carousel -->
            <jsp:include page="single-tour/img-carousel.jsp"/>
            <hr>
            <!-- description -->
                 <jsp:include page="single-tour/description.jsp"/>    
            <hr>
             <!-- schedule -->
             <jsp:include page="single-tour/schedule.jsp"/>    
            <hr>
             <!-- review -->
              <jsp:include page="single-tour/review.jsp"/>   
        </div><!--End  single_tour_desc-->
        
         <!-- booking -->
             <jsp:include page="single-tour/booking.jsp"/>   
    </div><!--End row -->
    </div><!--End container -->
