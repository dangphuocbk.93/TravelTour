<%-- 
    Document   : pagination
    Created on : May 6, 2017, 11:49:27 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>

<div class="text-center">
    <h1>${hql}</h1>
    
    <input type="hidden" name="hql" value="${hql}"/>
    <ul class="pagination">
        <li><a href="${pageContext.request.contextPath}/booking/tour/pagin.htm?page=${(page==1)? 1: (page-1)}${urlQuery}">Prev</a></li>
        <c:forEach var="i" begin="1" end="3"  >
           <li><a href="${pageContext.request.contextPath}/booking/tour/pagin.htm?page=${i}${urlQuery}">${i}</a></li>
        </c:forEach>
        <li><a href="${pageContext.request.contextPath}/booking/tour/pagin.htm?page=${(page==pagenumber)? pagenumber: (page+1)}${urlQuery}">Next</a></li>
    </ul>
    
</div>
