<%-- 
    Document   : sort
    Created on : May 1, 2017, 5:46:07 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<div id="tools">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="styled-select-filters">
                <select  onchange="sort()" name="sort" id="sort">              
                    <option value="" selected>Sort by price</option>
                    <option value="lower">Lowest price</option>
                    <option value="higher">Highest price</option>
                </select>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="styled-select-filters">
                <select  name="sort_rating" id="sort_rating">
                    <option value="" selected>Sort by ranking</option>
                    <option value="lower">Lowest ranking</option>
                    <option value="higher">Highest ranking</option>
                </select>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 hidden-xs text-right">
            <a href="all_tours_grid.html" class="bt_filters"><i class="icon-th"></i></a> <a href="#" class="bt_filters"><i class=" icon-list"></i></a>
        </div>
    </div>
</div>

<script>
    function sort() {
       var e = document.getElementById("sort").value;
        window.location.href = "${pageContext.request.contextPath}/booking/tour/sort.htm?page=1&sort="+e;
    }
</script>
