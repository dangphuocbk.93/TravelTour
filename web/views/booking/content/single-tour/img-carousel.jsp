<%-- 
    Document   : img-carousel
    Created on : May 10, 2017, 8:48:11 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<div id="Img_carousel" class="slider-pro">
                <div class="sp-slides">
                    <c:forEach var="ha" items="${lstha}">
                       <div class="sp-slide">
                        <img alt="Image" class="sp-image" src="css/images/blank.gif" 
                             data-src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${id}/${ha.hinhanh}" 
                        data-small="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${id}/${ha.hinhanh}" 
                        data-medium="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${id}/${ha.hinhanh}" 
                        data-large="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${id}/${ha.hinhanh}" 
                        data-retina="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${id}/${ha.hinhanh}">
                    </div> 
                    </c:forEach>
                    
                </div>
               
    <div class="sp-thumbnails">
        <c:forEach var="ha" items="${lstha}"> 
              <img alt="Image" class="sp-thumbnail" src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${id}/${ha.hinhanh}">
        </c:forEach>
                  
                    
                </div>
            </div>
