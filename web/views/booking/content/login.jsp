<%-- 
    Document   : login
    Created on : Apr 8, 2017, 10:10:32 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<section id="hero" class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div id="login">
                    <div class="text-center"><img src="img/logo_sticky.png" alt="Image" data-retina="true" ></div>
                    <hr>
                    <f:form action="${pageContext.request.contextPath}/booking/master/login.htm" commandName="nd" method="post">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 login_social">
                                <a href="#" class="btn btn-primary btn-block"><i class="icon-facebook"></i> Facebook</a>
                            </div>

                            <div class="col-md-6 col-sm-6 login_social">
                                <a href="#" class="btn btn-info btn-block "><i class="icon-twitter"></i>Twitter</a>
                            </div>
                        </div> <!-- end row -->
                        <div class="login-or"><hr class="hr-or"><span class="span-or">or</span></div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class=" form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class=" form-control" placeholder="Password">
                        </div>
                        <p class="small">
                            <a href="#">Forgot Password?</a>
                        </p>
                        <input type="submit" name="Sign_in" value="Login" id="Sign_in" class="btn_full"/>
                        <a href="${pageContext.request.contextPath}/booking/master/register.htm " class="btn_full_outline">Register</a>
                    </f:form>      
                
                </div>
            </div>
        </div>
    </div>
</section>