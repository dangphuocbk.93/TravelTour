<%-- 
    Document   : login
    Created on : Apr 8, 2017, 10:34:14 AM
    Author     : Phuoc
--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>
  <head>
    <!--Khai báo đường dẫn gốc để load file css/js/img -->
    <base href="${pageContext.request.contextPath}/sources/booking/">
    <meta http-equiv="Content-Type" content="text/html" charset= "UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Citytours - Premium site template for city tours agencies, transfers and tickets.">
    <meta name="author" content="Ansonika">
    <title>CITY TOURS - City tours and travel site template by Ansonika</title>
    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
    <!-- BASE CSS -->
    <link href="css/base.css" rel="stylesheet">
     <!-- CSS -->
    <link href="css/flickity.css" rel="stylesheet">
    <!-- Google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
    <!-- REVOLUTION SLIDER CSS -->
    <link href="css/settings.css" rel="stylesheet">
    <link href="css/extralayers.css" rel="stylesheet">
    <!-- SLIDER REVOLUTION 4.x SCRIPTS -->
    <script src="js/jquery.themepunch.tools.min.js"></script>
    <script src="js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/revolution_func.js"></script>
     
  </head>
  <body>
    <!-- header -->
    <tiles:insertAttribute name="header"/>
     <!-- Main content -->
     <tiles:insertAttribute name="content"/>
    <!-- footer -->
    <tiles:insertAttribute name="footer"/>
    <!-- Common scripts -->
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/common_scripts_min.js"></script>
    <script src="js/functions.js"></script>
    <!-- Specific scripts -->
    <script src="js/pw_strenght.js"></script>
    
  </body>
</html>

