<%-- 
    Document   : menu
    Created on : Mar 4, 2017, 9:50:58 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-3">
                  <!--logo -->
                  <jsp:include page="logo.jsp"/>
                              
          </div>
          <nav class="col-md-9 col-sm-9 col-xs-9">
              <a href="#" class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
              <div class="main-menu">
                  <div id="header_menu">
                      <img src="img/logo_sticky.png" width="160" height="34" alt="City tours" data-retina="true"/>
                  </div>
                  <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                  <ul>
                      <li class="submenu">
                          <a href="#" class="show-submenu"><i class="icon-home"></i>Home</a>
                      </li>
                      <li class="submenu">
                           <a href="javascript:void(0);" class="show-submenu">Tour<i class="icon-building"></i></a>
                                  <ul>
                                      <li><a href="#"><i class="icon-columns"></i>Cập nhật Tour</a>
                                          <ul>
                                            <li><a href="#"><i class="icon-columns"></i>Thương hiệu</a></li>
                                            <li><a href="#"><i class="icon-columns"></i>Khu Vực</a></li>
                                            
                                          </ul>
                                      </li>
                                      <li><a href="#"><i class="icon-columns"></i>Tour mới</a></li>
                                      
                                  </ul>
                      </li>
                       <li class="submenu">
                          <a href="#" class="show-submenu">Hotels <i class="icon-bus"></i></a>
                      </li>
                      <li class="submenu">
                          <a href="#" class="show-submenu">Transfers <i class="icon-chart-bar-2"></i></a>
                      </li>
                        <li class="submenu">
                          <a href="http://youvivu.com/blog/" class="show-submenu">Travel Guides <i class="icon-book"></i></a>
                      </li>
                      <li class="submenu">
                          <a href="javascript:void(0);" class="show-submenu">More<i class="icon-down-open-mini"></i></a>
                                  <ul>
                                      <li><a href="#"><i class="icon-columns"></i>About Us</a></li>
                                      <li><a href="#"><i class="icon-columns"></i>Contact Us</a></li>
                                      <li><a href="#"><i class="icon-columns"></i>Term</a></li>
                                      <li><a href="#"><i class="icon-columns"></i>Support Center</a></li>
                                  </ul>
                      </li>
                  </ul>
              </div>
              <ul id="top_tools">
                  <li>
                   <!-- search -->
                   <jsp:include page="search.jsp"/>
                  </li>
                  <li>
                   <!-- cart -->
                   <jsp:include page="cart.jsp"/>
                  </li>
              </ul>
          </nav>
        </div>
      </div>
