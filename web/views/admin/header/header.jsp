<%-- 
    Document   : header
    Created on : Mar 23, 2017, 8:15:40 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header class="main-header">
    <!-- Logo -->
    <jsp:include page="logo.jsp"/>
    <!-- Header Navbar: style can be found in header.less --> 
    <nav class="navbar navbar-static-top">
     <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
     <!-- Sidebar toggle button-->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <jsp:include page="message.jsp" />
          <!-- Notifications: style can be found in dropdown.less -->
          <jsp:include page="notification.jsp"/>
          <!-- Tasks: style can be found in dropdown.less -->
          <jsp:include page="task.jsp"/>
          <!-- User Account: style can be found in dropdown.less -->
          <jsp:include page="user.jsp"/>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
