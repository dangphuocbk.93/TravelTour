<%-- 
    Document   : allkhuvuc
    Created on : Apr 1, 2017, 8:02:50 AM
    Author     : Phuoc
--%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>     
            Tour Data
            <small>All Tour</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Admin</a></li>
            <li class="active">Tour</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box">
                    <div class="box-header col-md-10">
                        <h3 class="box-title">Tour Data</h3>

                    </div>
                    <div class=" box-header col-md-2">

                        <!-- Trigger the modal with a button -->
                        <input type="button" class="btn btn-block btn-info btn-xs" data-toggle="modal" data-target="#myModal" value="Thêm mới" style="float: left" />
                        <!-- Modal -->
                        <f:form action="${pageContext.request.contextPath}/admin/khuvuc/insert.htm?page=${page}" commandName="kv" method="post">
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Tên khu vực</h4>
                                        </div>
                                        <div class="modal-body" >
                                            <p><input class="col-xs-6" type="text" name="tenkhuvuc" autocomplete="off"/></p>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit"  class="btn btn-default" value="Thêm mới" />
                                        </div>
                                    </div>

                                </div>
                            </div> 
                        </f:form>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Khu vực </th>
                                    <th>Cập nhật</th>
                                    <th>Xóa</th>
                                </tr>
                            </thead>
                            <tbody>                
                                <c:forEach var="kv" items="${lstkhuvuc}">
                                    <tr>
                                        <f:form action="${pageContext.request.contextPath}/admin/khuvuc/update.htm?page=${page}" modelAttribute="kv" method="post">
                                    <input type="hidden" name="id" value="${kv.id}"/>
                                    <td><input class="col-md-10" type="text" name="tenkhuvuc" value="${kv.tenkhuvuc}"/></td>

                                    <td>
                                        <input type="submit" class="btn btn-block btn-info btn-xs" value="Cập nhật" />
                                    </td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/admin/khuvuc/delete.htm?id_kv=${kv.id}&page=${page}">
                                            <input type="button" class="btn btn-block btn-danger btn-xs" value="Xóa"/>
                                        </a>
                                    </td>  
                                </f:form>

                                </tr>  
                            </c:forEach>
                            </tbody>
                        </table>
                        <div class="product-pagination text-center">                                  
                            <ul class="pagination">
                                <c:forEach var="i" begin="1" end="${pagenumber}">
                                    <li><a href="${pageContext.request.contextPath}/admin/khuvuc/getall.htm?page=${i}">${i}</a></li>
                                    </c:forEach>

                            </ul>                               
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->          
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>