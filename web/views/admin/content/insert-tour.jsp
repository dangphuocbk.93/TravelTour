<%-- 
    Document   : add-tour
    Created on : Apr 18, 2017, 9:34:27 PM
    Author     : Phuoc
--%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            General Form Elements
            <small>Preview</small>

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
        <f:form action="${pageContext.request.contextPath}/admin/tour/insert.htm" modelAttribute="tour" acceptCharset="utf-8" enctype="multipart/form-data">
            <input type="hidden" name="id" value="${tour.id}"/>
            <jsp:useBean id="date" class="java.util.Date"/>
            <c:set var="ngaytao"><fmt:formatDate value="${date}"/></c:set>    
            <input type="hidden" name="ngaytao" value="${ngaytao}" /><br>
            <input type="hidden" name="hienthi" value="1"/>     
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">                               
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thông tin chung</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->

                            <div class="box-body">
                                <div class="form-group">
                                    <label>Tourname</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tourname" name="tourname" value="${tour.tourname}" autocomplete="off" >  
                                </div>
                                <div class="form-group">
                                    <label>Số lượng người tham gia</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Số lượng người"  name="soluongnguoi" value="${tour.soluongnguoi}" autocomplete="off">             
                                </div>
                                <div class="form-group">
                                    <label>Khu vực</label>
                                    <select id="khuvuc" class="form-control" name="khuvuc.id">                            
                                        <c:forEach var="kv" items="${lstkv}" >
                                                    <option value="${kv.id}">${kv.tenkhuvuc}</option>   
                                        </c:forEach>                         
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Thương hiệu</label>
                                    <select id="khuvuc" class="form-control" name="thuonghieu.id">                            
                                        <c:forEach var="th" items="${lstth}" >
                                                    <option value="${th.id}">${th.tenthuonghieu}</option>                                  
                                        </c:forEach>                         
                                    </select>
                                </div>
                                <label>Giá</label>
                                <div class="input-group">
                                    <c:set var="dongia"><fmt:formatNumber value="${tour.dongia}" minFractionDigits="0"/></c:set>
                                    <input type="text" class="form-control" placeholder="Đơn giá" name="dongia" value="${tour.dongia}" autocomplete="off">
                                    <span class="input-group-addon">VND</span>
                                </div>
                                <label>Giá khuyến mãi</label>  
                                <div class="input-group">
                                    <c:set var="dongiaKm"><fmt:formatNumber value="${tour.dongiaKm}" minFractionDigits="0"/></c:set>
                                    <input type="text" class="form-control" placeholder="Đơn giá khuyến mãi" name="dongiaKm" value="${tour.dongiaKm}" autocomplete="off">
                                    <span class="input-group-addon">VND</span>
                                </div>
                                <div class="form-group">
                                    <label>Phương tiện</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Phương tiện" name="phuongtien" value="${tour.phuongtien}" autocomplete="off">  
                                </div>
                                <div class="form-group">
                                    <label>Hình ảnh</label>                                                                                                                      
                                    <img src="${pageContext.request.contextPath}/sources/admin/dist/img/${tour.id}/${tour.hinhanh}" class="img-rounded img-responsive" alt="Cinque Terre" width="250">                                          
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile" name="path">
                                    <input type="hidden" name="hinhanh" value="${tour.hinhanh}"/> 
                                </div>
                                <div class="box-footer">                                                                    
                                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                                    <a href="${pageContext.request.contextPath}/admin/img/getall.htm?id_tour=${tour.id}&page=1"><input type="button" class="btn btn-primary" value="Hình ảnh chi tiết" style="float: right"/></a>                                           
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div> 
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-body">
                                <!--Mô tả-->
                                <div class="form-group">
                                    <label>Giới thiệu Tour</label>
                                    <textarea class="form-control" rows="5" id="comment" placeholder="Giới thiệu tour" name="mota">${tour.mota}</textarea>
                                </div>
                                <!-- -->
                                <div class="box-header">
                                    <h3 class="box-title">Bootstrap WYSIHTML5
                                        <small>Simple and fast</small>
                                    </h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body pad">
                                    <form>
                                        <textarea class="textarea" placeholder="Lịch trình" name="lichtrinh" style="width: 100%; height: 505px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                            ${tour.lichtrinh}
                                        </textarea>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div> 
                </div>
            </section>
        </f:form>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
