<%-- 
    Document   : singletour
    Created on : Mar 23, 2017, 11:38:07 PM
    Author     : Phuoc
--%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            General Form Elements
            <small>Preview ${selectpage}</small>

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>         
            <!--Hình ảnh -->           
                <div class="row">
                    <div class="col-xs-12 col-md-6">                    
                        <!-- -->
                        <div class="box">
                            <f:form action="${pageContext.request.contextPath}/admin/img/add.htm?page=${page}" commandName="ha" acceptCharset="utf-8" enctype="multipart/form-data" >
                                <div class="box-header col-md-4">
                                    <h3 class="box-title">Images Detail</h3>                                                                                           
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile" name="path" >
                                    <input type="submit" value="Upload"/>  
                                    <input type="hidden" name="tour.id" value="${id_tour}"/>                                   
                                </div> 
                            </f:form>
                                   <div class="box-footer">                                                                    
                                            <a href="${pageContext.request.contextPath}/admin/tour/edit.htm?id_tour=${id_tour}"><input type="button" class="btn btn-primary" value="Thông tin" style="float: right"/></a>                                           
                                        </div>
                                         
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Hình ảnh</th>              
                                            <th style="text-align: center">Xóa</th>
                                        </tr>
                                    </thead>
                                    <tbody>  
                                        <c:forEach var="ha" items="${lstha}">
                                            <tr>
                                                <td><img src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${ha.tour.id}/${ha.hinhanh}" class="img-rounded" alt="Cinque Terre" width="200px"></td>
                                                <td>
                                                    <a href="${pageContext.request.contextPath}/admin/img/delete.htm?id_ha=${ha.id}&page=${page}"><input type="submit" onclick="confirm('Bạn có muốn xóa hình ảnh không?')" class="btn btn-block btn-danger btn-xs" value="Xóa"/></a>                                                                            
                                                </td>
                                            </tr> 
                                        </c:forEach>                                     
                                    </tbody>
                                </table>
                            </div>
                            <div class="product-pagination text-center">                                  
                                <ul class="pagination">
                                    <c:forEach var="i" begin="1" end="${pagenumber}">
                                         <li><a href="${pageContext.request.contextPath}/admin/img/getall.htm?id_tour=${id_tour}&page=${i}">${i}</a></li>
                                    </c:forEach>
                               
                                </ul>                               
                            </div>                       
                        </div>
                    </div>  
                </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->