<%-- 
    Document   : allkhuvuc
    Created on : Apr 1, 2017, 8:02:50 AM
    Author     : Phuoc
--%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>     
            Tour Data
            <small>All Tour</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Admin</a></li>
            <li class="active">Tour</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-10">
                <div class="box">
                    <div class="box-header col-md-10">
                        <h3 class="box-title">Tour Data</h3>
                    </div>
                    <div class=" box-header col-md-2">
                        <!-- Trigger the modal with a button -->
                        <input type="button" class="btn btn-block btn-info btn-xs" data-toggle="modal" data-target="#myModal" value="Thêm mới" style="float: left" />
                        <!-- Modal -->
                        <f:form action="${pageContext.request.contextPath}/admin/thuonghieu/insert.htm?page=${page}" commandName="th" method="post" enctype="multipart/form-data">
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Tên thương hiệu</h4>
                                        </div>
                                        <div class="modal-body" >
                                            <p><input class="col-xs-6" type="text" name="tenthuonghieu" autocomplete="off"/></p>

                                        </div>
                                        <div class="modal-body">
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile" name="path">
                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit"  class="btn btn-default" value="Thêm mới" />
                                        </div>
                                    </div>

                                </div>
                            </div> 
                        </f:form>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Thương hiệu </th>
                                    <th>Hình ảnh</th>
                                    <th>Cập nhật</th>
                                    <th>Xóa</th>
                                </tr>
                            </thead>
                            <tbody>                
                                <c:forEach var="th" items="${lstthuonghieu}">
                                    <tr>
                                        <f:form action="${pageContext.request.contextPath}/admin/thuonghieu/update.htm?page=${page}" modelAttribute="th" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="id" value="${th.id}"/>
                                    <td><input class="col-md-10" type="text" name="tenthuonghieu" value="${th.tenthuonghieu}"/></td>
                                    <td>                                                                                                                
                                        <img src="${pageContext.request.contextPath}/sources/admin/dist/img/thuonghieu/${th.id}/${th.hinhanh}" class="img-rounded img-responsive" alt="Cinque Terre" width="250">                                          
                                        <label for="exampleInputFile">File input</label>
                                        <input type="file" id="exampleInputFile" name="path">
                                        <input type="hidden" name="hinhanh" value="${th.hinhanh}"/> 
                                    </td>
                                    <td>
                                        <input type="submit" class="btn btn-block btn-info btn-xs" value="Cập nhật" />
                                    </td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/admin/thuonghieu/delete.htm?id_th=${th.id}&page=${page}">
                                            <input type="button" class="btn btn-block btn-danger btn-xs" value="Xóa"/>
                                        </a>
                                    </td>  
                                </f:form>
                                </tr>  
                            </c:forEach>
                            </tbody>
                        </table>
                        <div class="product-pagination text-center">                                  
                            <ul class="pagination">
                                <c:forEach var="i" begin="1" end="${pagenumber}">
                                    <li><a href="${pageContext.request.contextPath}/admin/thuonghieu/getall.htm?page=${i}">${i}</a></li>
                                    </c:forEach>

                            </ul>                               
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->          
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>