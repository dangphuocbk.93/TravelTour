/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/** 
 * Kiểm lỗi so khớp biểu thức chính qui 
 * @param value giá trị của trường cần kiểm lỗi 
 * @param element là thẻ cần kiểm lỗi 
 * @param expression biểu thức chính qui dùng để kiểm lỗi 
 */ 
function fnRegexValidator(value, element, expression) { 
  var regex = new RegExp(expression, "g"); 
    return this.optional(element) || regex.test(value); 
} 
 
/** 
 * Kiểm lỗi HTML 
 * @param value giá trị của trường cần kiểm lỗi 
 * @param element là thẻ cần kiểm lỗi 
 */ 
function fnTextValidator(value, element) { 
    return this.optional(element) || (value.indexOf("</") == -1); 
} 
 
/** 
 * Kiểm lỗi giới hạn tuổi của ngày sinh 
 * @param value giá trị của trường cần kiểm lỗi 
 * @param element là thẻ cần kiểm lỗi 
 * @param ages là mảng chưa phạm vi tuổi 
 */ 
function fnLimitValidator(value, element, ages) { 
  if(this.optional(element)){ 
    return true; 
  } 
  try{ 
          var age = new Date().getFullYear() - new Date(value).getFullYear(); 
      return (age >= ages[0] && age <= ages[1]); 
  } 
  catch (e) { 
    return false; 
  } 
} 
 
/*--Thêm luật kiểm lỗi mới--*/ 
$.validator.addMethod("regex", fnRegexValidator, "Not mactch with {0}."); 
$.validator.addMethod("text", fnTextValidator, "Accept only plain text."); 
$.validator.addMethod("limit", fnLimitValidator, "The age must be between {0} and {1}."); 
 
/*--Định nghĩa lại các thông báo lỗi mặc định--*/ 
jQuery.extend(jQuery.validator.messages, { 
    required: "Vui lòng không để trống !", 
    remote: "Kiểm lỗi từ xa thất bại !", 
    email: "Vui lòng nhập đúng định dạng email !", 
    url: "Vui lòng nhập đúng dạng địa chỉ website !", 
    date: "Vui lòng nhập đúng dạng ngày !", 
    dateISO: "Vui lòng nhập ngày dạng ISO !", 
    number: "Vui lòng nhập số !", 
    digits: "Vui lòng nhập số nguyên !", 
    creditcard: "Vui lòng nhập đúng dạng credit card !", 
    equalTo: "Vui lòng nhập giống giá trị gốc!", 
    accept: "Vui lòng chọn đúng dạng file !", 
    maxlength: jQuery.validator.format("Vui lòng nhập ít hơn {0} ký tự !"), 
    minlength: jQuery.validator.format("Vui lòng nhập ít nhất {0} ký tự !"), 
    rangelength: jQuery.validator.format("Vui lòng nhập số ký tự từ {0} đến {1} !"), 
    range: jQuery.validator.format("Vui lòng nhập giá trị từ {0} đến {1} !"), 
    max: jQuery.validator.format("Vui lòng nhập giá trị nhỏ hơn hoặc bằng {0} !"), 
    min: jQuery.validator.format("Vui lòng nhập giá trị lớn hơn hoặc bằng {0} !"), 
    regex: jQuery.validator.format("Vui lòng nhập giá trị khớp với {0} !"), 
    limit: jQuery.validator.format("Vui lòng nhập ngày có tuổi từ {0} đến {1} !"), 
    text: jQuery.validator.format("Vui lòng không nhập mã HTML !") 
}); 