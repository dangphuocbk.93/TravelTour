<%-- 
    Document   : reason
    Created on : Apr 8, 2017, 8:18:32 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<section class="margin_60 gray_bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 main_title">
                    <h2>Some <span>good</span> reasons</h2>
                    <p>Chúng tôi biết bạn có nhiều sự lựa chọn. Cảm ơn bạn đã tin dùng dịch vụ của chúng tôi.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>+120</span> Premium tours</h3>
                        <p>
                            Là đối tác của Du Lịch Việt, Rồng Á Châu, Du Lịch Thiên Nhiên chúng tôi tự tin mang lại cho khách hàng những sản phẩm tours du lịch chất lượng dịch vụ tốt nhất có thể.
                        </p>
                        <a href="about.html" class="btn_1 outline">Xem thêm</a>
                    </div>
                </div>
                <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span>+1000</span> Khách hàng</h3>
                        <p>
                            Youvivu luôn có giá tốt nhất đi kèm với chất lượng dịch vụ trên thị trường vì vậy chúng tôi đã được khách hàng tín dụng cùng trải nghiệm và đồng hành cùng tổ chức.
                        </p>
                        <a href="about.html" class="btn_1 outline">Xem thêm</a>
                    </div>
                </div>
                <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-57"></i>
                        <h3><span>H24 </span> Hổ trợ</h3>
                        <p>
                            Chúng tôi có đội ngũ nhân viên tư vấn du lịch và nhà hàng khách sạn chuyên nghiệp và thân thiện sẵn sàng hỗ trợ bạn từ khi đặt phòng cho đến lúc kết thúc chuyến đi.
                        </p>
                        <a href="about.html" class="btn_1 outline">Xem thêm</a>
                    </div>
                </div>
            </div>

            <hr/>
            <div class="row">
                <div class="col-md-8 col-sm-6 hidden-xs">
                    <img src="img/laptop.png" alt="Laptop" class="img-responsive laptop" />
                </div>
                <div class="col-md-4 col-sm-6">
                    <h3><span>Đồng hành</span> cùng Youvivu</h3>
                    <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.</p>
                    <ul class="list_order">
                        <li><span>1</span>Select your preferred tours</li>
                        <li><span>2</span>Purchase tickets and options</li>
                        <li><span>3</span>Pick them directly from your office</li>
                    </ul>
                    <a href="all_tour_list.html" class="btn_1">Start now</a>
                </div>
            </div>
        </div>
    </section>
