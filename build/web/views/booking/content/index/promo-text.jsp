<%-- 
    Document   : promo-text
    Created on : Apr 8, 2017, 8:17:16 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<section class="white_bg">
        <div class="container margin_60">
            <div class="row">
              <div class="col-md-3 col-sm-3 text-center">
                  <i class="icon-building main_middle_section_icon"></i>
                  <h4><span>Tours/ Hotels</span>booking</h4>
                  <p>Chúng tôi luôn có những khách sạn và tour đặc biệt mỗi ngày, không chỉ Giá tốt mà còn có những Dịch vụ độc, lạ, những Khuyến mãi hấp dẫn.</p>
              </div>

                <div class="col-md-3 col-sm-3 text-center">
                    <i class="icon-chart-bar-2 main_middle_section_icon"></i>
                    <h4><span>Activity</span> booking</h4>
                    <p>Hoạt động là những trải nghiệm cần thiết cho mỗi chuyến đi. Youvivu tin sẽ cung cấp cho bạn những hoạt đồng đầy trải nghiệm đáng nhớ.</p>
                </div>

                <div class="col-md-3 col-sm-3 text-center">
                    <i class="icon-bus main_middle_section_icon"></i>
                    <h4><span>Transfer</span> booking</h4>
                    <p>Chúng tôi cung cấp dịch vụ đưa đón bạn tận nơi, từ sân bay hay từ nhà bạn nhằm đem lại sự thoải mái nhất cho bạn có thể.</p>
                </div>

                <div class="col-md-3 col-sm-3 text-center">
                    <i class="icon-book main_middle_section_icon"></i>
                    <h4><span>Travel </span>guide</h4>
                    <p>Youvivu tự tin là thổ địa du lịch, cung cấp cho bạn những thông tin du lịch chọn lọc, độc đáo và mới nhất về tất cả các vùng miền qua Cẩm nang du lịch.</p>
                </div>
            </div>
        </div>
    </section>
