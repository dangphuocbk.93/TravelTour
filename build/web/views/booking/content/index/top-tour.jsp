<%-- 
    Document   : top-tour
    Created on : Apr 8, 2017, 8:19:10 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<div class="row">
    <c:forEach var="tour" items="${lstt}">
        <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                <div class="tour_container">
                	<div class="ribbon_3 popular"><span>Popular</span></div>
                    <div class="img_container">
                        <a href="single_tour.html">
                            <img src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${tour.id}/${tour.hinhanh}" class="img-responsive" alt="image" />
                        <div class="short_info">                        
                            <i class="icon_set_1_icon-26"></i>Tourist<span class="price"><sup>&#36;</sup>39</span>
                        </div>
                        
                        </a>
                    </div>
                        
                        <div class="tour_title" style="height: 110px">
                            <h3 style="width: 300px"><strong>${tour.tourname}</strong></h3>
                        <div class="rating">
                            <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i><small>(75)</small>
                        </div>
                        <div class="wishlist">
                            <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                        </div>
                    </div>
                </div>
            </div>
    </c:forEach>
      </div>
