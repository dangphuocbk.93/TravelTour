<%-- 
    Document   : sumary
    Created on : May 13, 2017, 11:44:17 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<aside class="col-md-4">
    <div class="box_style_1">
        <h3 class="inner">- Summary -</h3>
        <table class="table table_summary">
        <tbody>
                    <tr>

            <td>
                Số tour
            </td>
            <td class="text-right">
                ${sum}
            </td>
        </tr>
        <tr>

            <td>
                Người lớn
            </td>
            <td class="text-right">
                ${slnl}
            </td>
        </tr>
        <tr>
            <td>
                Children
            </td>
            <td class="text-right">
                ${slte}
            </td>
        </tr>
        <tr class="total">
            <td>
                Tổng
            </td>
            <td class="text-right">
                <strong><fmt:formatNumber value="${total}"/> VNĐ</strong>
            </td>
        </tr>
        </tbody>
        </table>
        <a class="btn_full" href="payment.html">Check out</a>
        <a class="btn_full_outline" href="${pageContext.request.contextPath}/booking/tour/list-tour.htm?page=1"><i class="icon-right"></i> Continue shopping</a>
    </div>
    <div class="box_style_4">
        <i class="icon_set_1_icon-57"></i>
        <h4>Need <span>Help?</span></h4>
        <a href="tel://004542344599" class="phone">+45 423 445 99</a>
        <small>Monday to Friday 9.00am - 7.30pm</small>
    </div>
    </aside><!-- End aside -->
