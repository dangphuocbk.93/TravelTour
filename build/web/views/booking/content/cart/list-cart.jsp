<%-- 
    Document   : list-cart
    Created on : May 13, 2017, 11:39:11 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<table class="table table-striped cart-list add_bottom_30">
            <thead>
            <tr>
                <th>
                    Tour name 
                </th>
                <th>
                    Số người lớn 
                </th>
                    <th>
                    Số trẻ em 
                </th>
                <th>
                    Tổng cộng (VNĐ)
                </th>
                <th>
                    Tùy chỉnh
                </th>
            </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${listItem}">
                <form action="${pageContext.request.contextPath}/booking/cart/action.htm" id="action">
                   
                    <tr>
                        <input type="hidden" name="id" value="${item.tour.id}"/>
                <td>
                    <div class="thumb_cart">
                        <img src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${item.tour.id}/${item.tour.hinhanh}" alt="Image">
                    </div>
                    <span class="item_cart" style="width: 180px">${item.tour.tourname}</span>
                </td>
                <td>
                    <div class="numbers-row">
                        <input type="text" value="${item.slNguoilon}" id="quantity_3" class="qty2 form-control" name="q1">
                    </div>
                </td>
                  <td>
                    <div class="numbers-row">
                        <input type="text" value="${item.slTreem}" id="quantity_3" class="qty2 form-control" name="q2">
                    </div>
                </td>
                <td>
                    <strong><fmt:formatNumber value="${item.slNguoilon*item.tour.dongiaKm +(0.5)*item.slTreem*item.tour.dongiaKm}"/></strong>
                </td>
                <td class="options">
                    
                    <button class="btn btn-default btn-xs" name="action" value="update"><a><i class="icon-ccw-2"></i></a></button>
                    <button class="btn btn-default btn-xs" name="action" value="delete"><a><i class="icon-trash"></i></a></button>
                </td>
            </tr>
            </form> 
                </c:forEach>
                   
    
            
            
            </tbody>
            </table>
