<%-- 
    Document   : main-content
    Created on : Apr 8, 2017, 8:20:22 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div>
   <div>
    <div class="container margin_60">

        <jsp:include page="index/main-title.jsp"/>
        <jsp:include page="index/top-tour.jsp"/>
        <jsp:include page="index/view-alltour.jsp"/>
    </div>
    <jsp:include page="index/promo-text.jsp"/>
    <jsp:include page="index/promo-video.jsp"/>
    <jsp:include page="index/reason.jsp"/>
</div>
</div>
