<%-- 
    Document   : booking
    Created on : May 10, 2017, 8:56:41 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<aside class="col-md-4">
    <p class="hidden-sm hidden-xs">
        <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap">View on map</a>
    </p>
    <form  action="${pageContext.request.contextPath}/booking/cart/add.htm">
        <input type="hidden" name="id" value="${tour.id}"/>
        <div class="box_style_1 expose">
            <h3 class="inner">- Booking -</h3>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label><i class="icon-calendar-7"></i>Chọn ngày</label>
                        <input class="date-pick form-control" data-date-format="M d, D" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label>Người lớn</label>
                        <div class="numbers-row">
                            <input type="text" value="1" id="adults" class="qty2 form-control" name="q1">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label>Trẻ em</label>
                        <div class="numbers-row">
                            <input type="text" value="0" id="children" class="qty2 form-control" name="q2">
                        </div>
                    </div>
                    <h6 style="color: #f59e00">Giá trẻ em = 50% người lớn</h6>
                </div>
            </div>
            <br>
            <table class="table table_summary">
                <tbody>
                    <tr class="total">
                        <td>
                            Giá Tour
                        </td>
                        <td class="text-right">
                            <strong><fmt:formatNumber value="${tour.dongiaKm}"/> VNĐ</strong><br>
                        </td>
                    </tr>
                </tbody>
            </table>
                        <input class="btn_full" type="submit" value="Book now" />
            <a class="btn_full_outline" href="#"><i class=" icon-heart"></i> Add to whislist</a>
            
        </div><!--/box_style_1 -->
    </form>

    <div class="box_style_4">
        <i class="icon_set_1_icon-90"></i>
        <h4><span>Book</span> by phone</h4>
        <a href="tel://004542344599" class="phone">+45 423 445 99</a>
        <small>Monday to Friday 9.00am - 7.30pm</small>
    </div>

</aside>