<%-- 
    Document   : cart
    Created on : May 13, 2017, 11:37:34 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container margin_60">
    <div class="row">
    <div class="col-md-8">
    	<!-- list-cart -->
        <jsp:include page="cart/list-cart.jsp"/>
    </div><!-- End col-md-8 -->
    	<!-- aside-summary -->
     <jsp:include page="cart/summary.jsp"/>

</div><!--End row -->
</div><!--End container -->