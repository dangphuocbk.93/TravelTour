<%-- 
    Document   : register
    Created on : Apr 8, 2017, 10:44:25 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<section id="hero" class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div id="login">
                    <div class="text-center"><img src="img/logo_sticky.png" alt="Image" data-retina="true" ></div>
                    <hr>


                    <f:form action="${pageContext.request.contextPath}/booking/master/register_finish.htm" commandName="nd" method="post">
                        <div class="form-group">
                            <input type="hidden" name="vaitro.id" value="1" class=" form-control"  placeholder="Username">
                        </div>   

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class=" form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class=" form-control" id="password1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label>Confirm password</label>
                            <input type="password" class=" form-control" id="password2" placeholder="Confirm password">
                        </div>
                        <div id="pass-info" class="clearfix"></div>
                        <button class="btn_full">Create an account</button>

                    </f:form>
                         <h6 style="color: #00acd6">${message}</h6>

                </div>
            </div>
        </div>
    </div>
</section>
