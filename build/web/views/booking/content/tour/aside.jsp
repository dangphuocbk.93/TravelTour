<%-- 
    Document   : aside
    Created on : May 1, 2017, 5:38:52 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<aside class="col-lg-3 col-md-3">
            <p>
                <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap">View on map</a>
            </p>
            <div class="box_style_cat">
                <ul id="cat_nav">
                    <li><a href="#" id="active"><i class="icon_set_1_icon-51"></i>All tours <span>(141)</span></a></li>
                </ul>
            </div>
            <f:form action="${pageContext.request.contextPath}/booking/tour/filter.htm?page=1">
                <div id="filters_col">
                <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt"><i class="icon_set_1_icon-65"></i>Filters <i class="icon-plus-1 pull-right"></i></a>
                <div class="collapse" id="collapseFilters">
                    <div class="filter_type">
                        <h6>Price</h6>
                        <input type="text" id="range" name="range" value="">
                    </div>
                    <div class="filter_type">
                        <h6>Rating</h6>
                        <ul>
                            <li><label><input type="checkbox"><span class="rating">
                                        <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i>
                                    </span></label></li>
                            <li><label><input type="checkbox"><span class="rating">
                                        <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i>
                                    </span></label></li>
                            <li><label><input type="checkbox"><span class="rating">
                                        <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i>
                                    </span></label></li>
                            <li><label><input type="checkbox"><span class="rating">
                                        <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i>
                                    </span></label></li>
                            <li><label><input type="checkbox"><span class="rating">
                                        <i class="icon-star voted"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i>
                                    </span></label></li>
                        </ul>
                    </div>
                    <div class="filter_type">
                        <h6>Thương hiệu</h6>
                        <ul>
                            <c:forEach var="th" items="${lstth}">
                                <li><label><input name="thuonghieu" type="checkbox" value="${th.tenthuonghieu}">${th.tenthuonghieu}</label></li>
                            </c:forEach>                           
                        </ul>
                    </div>
                       <div class="filter_type">
                        <h6>Khu vực</h6>
                        <ul>
                            <c:forEach var="kv" items="${lstkv}">
                                <li><label><input name="khuvuc" type="checkbox" value="${kv.tenkhuvuc}">${kv.tenkhuvuc}</label></li>
                            </c:forEach>                          
                        </ul>
                    </div>
                    <input type="submit" class="btn btn-info" value="Lọc">
                </div><!--End collapse -->
            </div><!--End filters col-->
            </f:form>
                
       
            
            <div class="box_style_2">
                <i class="icon_set_1_icon-57"></i>
                <h4>Need <span>Help?</span></h4>
                <a href="tel://004542344599" class="phone">+45 423 445 99</a>
                <small>Monday to Friday 9.00am - 7.30pm</small>
            </div>
          
        </aside><!--End aside -->
