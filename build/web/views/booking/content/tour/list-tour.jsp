<%-- 
    Document   : list-tour
    Created on : May 1, 2017, 5:48:20 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>

<c:forEach var="tour" items="${lstt}">
    <div class="strip_all_tour_list wow fadeIn" data-wow-delay="0.1s">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="wishlist">
                    <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span class="tooltip-content-flip"><span class="tooltip-back">Add to wishlist</span></span></a>
                </div>      
                <div class="img_list"><a href="single_tour.html"><div class="ribbon popular" ></div><img src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${tour.id}/${tour.hinhanh}" alt="Image">
                        <div class="short_info"><i class="icon_set_1_icon-4"></i>${tour.thuonghieu.tenthuonghieu} </div></a>
                </div>
            </div>
            <div class="clearfix visible-xs-block"></div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="tour_list_desc">
                    <div class="rating"><i class="icon-star voted"></i><i class="icon-star  voted"></i><i class="icon-star  voted"></i><i class="icon-star  voted"></i><i class="icon-star-empty"></i><small>(75)</small></div>
                    <h3><strong>${tour.tourname}</strong></h3>
                    <p>${tour.mota.substring(0,100)}...</p>
                    <ul class="add_info">
                         <li>
                            <div class="tooltip_styled tooltip-effect-4">
                                <span class="tooltip-item"><i class="icon_set_1_icon-41"></i></span>
                                <div class="tooltip-content"><h4>Khu vực</h4>
                                    <strong>${tour.khuvuc.tenkhuvuc}</strong><br>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="tooltip_styled tooltip-effect-4">
                                <span class="tooltip-item"><i class=" icon_set_1_icon-36"></i></span>
                                <div class="tooltip-content"><h4>Đơn giá</h4>
                                    <strong><fmt:formatNumber value="${tour.dongiaKm}"/> VNĐ</strong><br>
                                    <strong style="text-decoration: line-through"><fmt:formatNumber value="${tour.dongia}"/> VNĐ</strong><br>
                                </div>
                            </div>
                        </li>
                              <li>
                            <div class="tooltip_styled tooltip-effect-4">
                                <span class="tooltip-item"><i class="icon_set_1_icon-25"></i></span>
                                <div class="tooltip-content"><h4>Phương tiện</h4>
                                    <strong>${tour.phuongtien}</strong><br>
                                </div>
                            </div>
                        </li>
                    </ul>
                    
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="price_list"><div>             
                        <p><a href="${pageContext.request.contextPath}/booking/tour/singletour.htm?id=${tour.id}" class="btn_1">Details</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div><!--End strip -->
    <hr>
</c:forEach>

        
