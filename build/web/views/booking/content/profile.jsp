<%-- 
    Document   : profile
    Created on : Apr 9, 2017, 11:58:51 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<div class="container">
    <section id="section-4">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <h4>Your profile</h4>
                <ul id="profile_summary">
                    <li>Họ tên <span>${nd.hoten}</span></li>
                    <li>Email <span>${nd.email}</span></li>
                    <li>Địa chỉ <span>${nd.diachi}</span></li>
                    <li>Số điện thoại <span>${nd.dtdd}</span></li>              
                </ul>
            </div>
            <div class="col-md-6 col-sm-6">
                <img src="${pageContext.request.contextPath}/sources/booking/img/nguoidung/${nd.hinhanh}" alt="Image" class="img-responsive styled profile_pic">
            </div>
        </div><!-- End row -->

        <f:form action="${pageContext.request.contextPath}/booking/user/update.htm" modelAttribute="nd" method="post" acceptCharset="utf-8" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-12">
                <h4>Edit profile</h4>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Họ tên</label>
                    <input class="form-control" name="hoten" id="first_name" type="text" value="${nd.hoten}"/>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" id="last_name" type="text" value="${nd.email}" readonly="readonly"/>
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Địa chỉ</label>
                    <input class="form-control" name="diachi" id="first_name" type="text" value="${nd.diachi}"/>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Số điện thoại</label>
                    <input class="form-control" name="dtdd" id="last_name" type="text" value="${nd.dtdd}"/>
                </div>
            </div>
        </div><!-- End row -->
        <input type="hidden" name="id" value="${nd.id}"/>
        <input type="hidden" name="vaitro.id" value="${nd.vaitro.id}"/>
        <input type="hidden" name="password" value="${nd.password}"/>        
        <hr>
        <h4>Upload profile photo</h4>
        <div class="form-inline upload_1">
            <div class="form-group">
                <input type="file" name="path" id="js-upload-files" multiple>
            </div>
        </div>
            <hr>
            <button type="submit" class="btn_1 green">Update Profile</button>
            <hr>
             </f:form>
    </section><!-- End section 4 -->
</div>
