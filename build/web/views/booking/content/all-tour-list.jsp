<%-- 
    Document   : all-tour-list
    Created on : May 1, 2017, 5:27:40 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
    <div  class="container margin_60">
    <div class="row">
        <!-- aside -->
        <jsp:include page="tour/aside.jsp"/>
        <!--End aside -->
        <div class="col-lg-9 col-md-9">
            <!--sort-->
            <jsp:include page="tour/sort.jsp"/>          
            <!--/sort -->
            <!-- List tour-->
            <jsp:include page="tour/list-tour.jsp"/>  
            <jsp:include page="tour/pagin.jsp"/>
            <!-- End list tour-->
        </div><!-- End col lg-9 -->
    </div><!-- End row -->
</div><!-- End container -->


