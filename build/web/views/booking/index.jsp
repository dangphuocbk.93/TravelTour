
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <!--Khai báo đường dẫn gốc để load file css/js/img -->
        <base href="${pageContext.request.contextPath}/sources/booking/">
        <meta http-equiv="Content-Type" content="text/html" charset= "UTF-8" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Citytours - Premium site template for city tours agencies, transfers and tickets.">
        <meta name="author" content="Ansonika">
        <title>CITY TOURS - City tours and travel site template by Ansonika</title>
        <!-- Favicons-->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
        <!-- BASE CSS -->
        <link href="css/base.css" rel="stylesheet">
        <!-- Google web fonts -->
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
        <!-- REVOLUTION SLIDER CSS -->
        <link href="css/settings.css" rel="stylesheet">
        <link href="css/extralayers.css" rel="stylesheet">
        <!-- SLIDER REVOLUTION 4.x SCRIPTS -->
        <script src="js/jquery.themepunch.tools.min.js"></script>
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/revolution_func.js"></script>
        <!-- Radio and check inputs -->
        <link href="css/skins/square/grey.css" rel="stylesheet">
        <!-- Range slider -->
        <link href="css/ion.rangeSlider.css" rel="stylesheet" >
        <link href="css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
         <!-- img carousel -->
        <link href="css/slider-pro.min.css" rel="stylesheet">
     <link href="css/date_time_picker.css" rel="stylesheet">

    </head>
    <body>
        <!-- header -->
        <tiles:insertAttribute name="header"/>
        <!-- slider -->
        <tiles:insertAttribute name="slider"/>
        <!-- Main content -->
        <tiles:insertAttribute name="content"/>
        <!-- footer -->
        <tiles:insertAttribute name="footer"/>
        <!-- Common scripts -->
        <script src="js/jquery-1.11.2.min.js"></script>
        <script src="js/common_scripts_min.js"></script>
        <script src="js/functions.js"></script>
        <!-- Check and radio inputs -->
        <script src="js/icheck.js"></script>
        <script>
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-grey',
                radioClass: 'iradio_square-grey'
            });
        </script>
        <!-- Date and time pickers -->
        <script src="js/jquery.sliderPro.min.js"></script>
        <script type="text/javascript">
                    $(document).ready(function ($) {
                        $('#Img_carousel').sliderPro({
                            width: 960,
                            height: 500,
                            fade: true,
                            arrows: true,
                            buttons: false,
                            fullScreen: false,
                            smallSize: 500,
                            startSlide: 0,
                            mediumSize: 1000,
                            largeSize: 3000,
                            thumbnailArrows: true,
                            autoplay: false
                        });
                    });
        </script>

        <!-- Date and time pickers -->
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script>
                   $('input.date-pick').datepicker('setDate', 'today');
                   $('input.time-pick').timepicker({
                       minuteStep: 15,
                       showInpunts: false
                   });
        </script>

        <!--Review modal validation -->
        <script src="assets/validate.js"></script>
    </body>
</html>
