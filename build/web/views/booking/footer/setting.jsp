<%-- 
    Document   : setting
    Created on : Mar 4, 2017, 10:06:51 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="col-md-2 col-sm-3">
          <h3>Cài đặt</h3>
          <div class="styled-select">
              <select class="form-control" name="lang" id="lang">
                  <option value="English">English</option>
                  <option value="French">French</option>
                  <option value="Spanish">Spanish</option>
                  <option value="Russian">Russian</option>
              </select>
          </div>
          <div class="styled-select">
              <select class="form-control" name="currency" id="currency">
                  <option value="USD">USD</option>
                  <option value="EUR">EUR</option>
                  <option value="GBP">GBP</option>
                  <option value="RUB">RUB</option>
              </select>
          </div>
      </div>
