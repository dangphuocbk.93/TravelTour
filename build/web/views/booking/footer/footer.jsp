<%-- 
    Document   : footer
    Created on : Mar 4, 2017, 10:08:26 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<footer>
          <div class="container">
            <div class="row">
                <jsp:include page="help.jsp"/>
                <jsp:include page="info.jsp"/>
                <jsp:include page="about.jsp"/>
                <jsp:include page="setting.jsp"/>
            </div>
            <div class="row">
                <jsp:include page="social.jsp"/>
            </div>
          </div>
        </footer>