<%-- 
    Document   : topline
    Created on : Mar 4, 2017, 9:52:22 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="top_line">
              <div class="container">
                  <div class="row">
                         <!-- switchlocation -->
                      <jsp:include page="switchlocation.jsp"/>
                         <!-- hotline-->
                      <jsp:include page="hotline.jsp"/>
                      <div class="col-md-4 colsm-4 col-xs-4">
                          <ul id="top_links">
                              <li>
                                     <!-- login-->
                                  <jsp:include page="login.jsp"/>
                              </li>
                              <li>  
                                     <!-- wishlist-->
                                  <jsp:include page="wishlist.jsp"/>  
                              
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>