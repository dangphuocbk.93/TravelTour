<%-- 
    Document   : login
    Created on : Mar 4, 2017, 9:50:28 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>

<div class="dropdown dropdown-access">

    <c:choose>
        <c:when test="${sessionScope.nd.id>0}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">${sessionScope.nd.email}</a>            
            <div class="dropdown-menu">
                <div class="login-or">
                    <img src="${pageContext.request.contextPath}/sources/booking/img/nguoidung/${sessionScope.nd.hinhanh}" alt="Image" class="img-responsive img-circle">                  
                    <hr class="hr-or"/>
                </div>
                <a href="${pageContext.request.contextPath}/booking/master/logout.htm"><input type="submit" value="Log out" id="log_out" class="button_drop"/></a>
                <a href="${pageContext.request.contextPath}/booking/user/profile.htm"><input type="submit" value="Profile"  class="button_drop outline"/></a>
            </div>
        </c:when>
        <c:otherwise>
            <f:form action="${pageContext.request.contextPath}/booking/master/login.htm" commandName="nd" method="post">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="access_link">Login</a>
                <div class="dropdown-menu">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#" class="bt_facebook">
                                <i class="icon-facebook"></i>Facebook </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <a href="#" class="bt_google">
                                <i class="icon-google"></i>Google +</a>
                        </div>
                    </div>
                    <div class="login-or">
                        <hr class="hr-or"/>
                        <span class="span-or">or</span>
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" id="inputUsernameEmail" placeholder="Email"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="password"/>
                    </div>
                    <a id="forgot_pw" href="#">Forget password &#63; </a>
                    <input type="submit" name="Sign_in" value="Login" id="Sign_in" class="button_drop"/>
                    <a href="${pageContext.request.contextPath}/booking/master/signup.htm"><input type="submit" name="Sign_up" value="Sign Up" id="Sign_up" class="button_drop outline"/></a>
                </div>
            </f:form>
        </c:otherwise> 
    </c:choose>


</div>
