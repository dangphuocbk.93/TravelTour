<%-- 
    Document   : search
    Created on : Mar 4, 2017, 9:51:27 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="dropdown dropdown-search">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></a>
          <div class="dropdown-menu">
              <form>
                  <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search..."/>
                      <span class="input-group-btn">
                      <button class="btn btn-default" type="button">
                      <i class="icon-search"></i>
                      </button>
                      </span>
                  </div>
              </form>
          </div>
      </div>