<%-- 
    Document   : slider
    Created on : Mar 4, 2017, 10:02:59 AM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-caption">
          <div class="intro_title">
    	     <h3 class="animated fadeInDown">Discovery Southeast Asia</h3>
           <p class="animated fadeInDown">Hotels Booking / Tours Booking / Tour Guides</p>
           <a href="#" class="animated fadeInUp button_intro">View Tours</a> <a href="#" class="animated fadeInUp button_intro outline">View Hotels</a>
	       </div>
        </div>
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="img/slider/slider_1.jpg" alt="Chania"/>
    </div>
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
