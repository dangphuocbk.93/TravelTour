<%-- 
    Document   : admin
    Created on : Mar 23, 2017, 9:27:53 PM
    Author     : Phuoc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="${pageContext.request.contextPath}/admin/tour/getall.htm?page=1"><i class="fa fa-circle-o"></i>Tour</a></li>
            <li><a href="${pageContext.request.contextPath}/admin/khuvuc/getall.htm?page=1"><i class="fa fa-circle-o"></i>Khu vực</a></li>
            <li><a href="${pageContext.request.contextPath}/admin/thuonghieu/getall.htm?page=1"><i class="fa fa-circle-o"></i>Thương hiệu</a></li>
          </ul>
        </li>
