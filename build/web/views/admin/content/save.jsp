<%-- 
    Document   : singletour
    Created on : Mar 23, 2017, 11:38:07 PM
    Author     : Phuoc
--%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            General Form Elements
            <small>${message}</small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
                <f:form action="${pageContext.request.contextPath}/admin/tour/update.htm" modelAttribute="tour" acceptCharset="utf-8" enctype="multipart/form-data">
                    <section class="content">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-6">                               
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Thông tin chung</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <!-- form start -->

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Tourname</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tourname" name="tourname" value="${tour.tourname}" >  
                                        </div>
                                        <div class="form-group">
                                            <label>Số lượng người tham gia</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Số lượng người"  name="soluongnguoi" value="${tour.soluongnguoi}">             
                                        </div>
                                        <div class="form-group">
                                            <label>Khu vực</label>
                                            <select id="khuvuc" class="form-control" name="khuvuc.id">                            
                                                <c:forEach var="kv" items="${lstkv}" >
                                                    <c:choose>
                                                        <c:when test="${kv.tenkhuvuc==tour.khuvuc.tenkhuvuc}">
                                                            <option  selected="selected" value="${kv.id}">${kv.tenkhuvuc}</option>  
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${kv.id}">${kv.tenkhuvuc}</option>   
                                                        </c:otherwise>
                                                    </c:choose>               
                                                </c:forEach>                         
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Thương hiệu</label>
                                            <select id="khuvuc" class="form-control" name="thuonghieu.id">                            
                                                <c:forEach var="th" items="${lstth}" >
                                                    <c:choose>
                                                        <c:when test="${th.tenthuonghieu==tour.thuonghieu.tenthuonghieu}">
                                                            <option  selected="selected" value="${th.id}">${th.tenthuonghieu}</option>  
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${th.id}">${th.tenthuonghieu}</option>  
                                                        </c:otherwise>
                                                    </c:choose>                                    
                                                </c:forEach>                         
                                            </select>
                                        </div>
                                        <label>Giá</label>
                                        <div class="input-group">
                                            <c:set var="dongia"><fmt:formatNumber value="${tour.dongia}" minFractionDigits="0"/></c:set>
                                            <input type="text" class="form-control" name="dongia" value="${tour.dongia}">
                                            <span class="input-group-addon">VND</span>
                                        </div>
                                        <label>Giá khuyến mãi</label>  
                                        <div class="input-group">
                                            <c:set var="dongiaKm"><fmt:formatNumber value="${tour.dongiaKm}" minFractionDigits="0"/></c:set>
                                            <input type="text" class="form-control" name="dongiaKm" value="${tour.dongiaKm}">
                                            <span class="input-group-addon">VND</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Phương tiện</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Phương tiện" name="phuongtien" value="${tour.phuongtien}" >  
                                        </div>
                                        <div class="form-group">
                                            <label>Hình ảnh</label>                                                                                                                      
                                            <img src="${pageContext.request.contextPath}/sources/admin/dist/img/tour/${tour.id}/${tour.hinhanh}" class="img-rounded img-responsive" alt="Cinque Terre" width="250">                                          
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile" name="path">
                                            <input type="hidden" name="hinhanh" value="${tour.hinhanh}"/> 
                                        </div>
                                        <div class="box-footer">                                                                    
                                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                                            <a href="${pageContext.request.contextPath}/admin/img/getall.htm?id_tour=${tour.id}&page=1"><input type="button" class="btn btn-primary" value="Hình ảnh chi tiết" style="float: right"/></a>                                           
                                        </div>
                                         
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div> 
                            <div class="col-md-6">
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <!--Mô tả-->
                                        <div class="form-group">
                                            <label>Giới thiệu Tour</label>
                                            <textarea class="form-control" rows="5" id="comment" placeholder="Tourname" name="mota">${tour.mota}</textarea>
                                        </div>
                                        <!-- -->
                                        <div class="box-header">
                                            <h3 class="box-title">Bootstrap WYSIHTML5
                                                <small>Simple and fast</small>
                                            </h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body pad">
                                            <form>
                                                <textarea class="textarea" placeholder="Place some text here" name="lichtrinh" style="width: 100%; height: 505px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                    ${tour.lichtrinh}
                                                </textarea>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box -->
                            </div> 
                        </div>
                        <input type="hidden" name="id" value="${tour.id}"/>                      
                        <c:set var="ngaytao"><fmt:formatDate value="${tour.ngaytao}"/></c:set>    
                        <input type="hidden" name="ngaytao" value="${ngaytao}" /><br>
                        <input type="hidden" name="hienthi" value="${tour.hienthi}"/>
                      
                    </section>
                </f:form>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->