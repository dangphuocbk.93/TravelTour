<%-- 
    Document   : tour
    Created on : Mar 23, 2017, 9:35:36 PM
    Author     : Phuoc
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>     
            Tour Data
            <small>All Tour</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Admin</a></li>
            <li class="active">Tour</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-md-10">
                        <h3 class="box-title">Tour Data</h3>
                         
                    </div>
                    <div class=" box-header col-md-2">
                        <a href="${pageContext.request.contextPath}/admin/tour/add.htm">
                        <input type="button" class="btn btn-block btn-info btn-xs" value="Thêm mới" style="float: left" />
                                            </a>  
                    </div>
                  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Tour name</th>
                                    <th>Khu vực</th>
                                    <th>Thương hiệu</th>
                                    <th>Cập nhật</th>
                                    <th>Xóa</th>
                                </tr>
                            </thead>
                            <tbody>                
                                <c:forEach var="tour" items="${lsttour}">
                                    <tr>
                                        <td>${tour.tourname}</td>
                                        <td>${tour.khuvuc.tenkhuvuc}</td>
                                        <td>${tour.thuonghieu.tenthuonghieu}</td>
                                        <td>
                                            <a href="${pageContext.request.contextPath}/admin/tour/edit.htm?id_tour=${tour.id}">
                                                <input type="button" class="btn btn-block btn-info btn-xs" value="Cập nhật" />
                                            </a>
                                        </td>
                                        <td>
                                            <a href="${pageContext.request.contextPath}/admin/tour/delete.htm?id_tour=${tour.id}&page=${page}">
                                                <input type="button"  class="btn btn-block btn-danger btn-xs" value="Xóa"/>
                                            </a>
                                        </td>
                                    </tr>  
                                </c:forEach>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Tour name</th>
                                    <th>Khu vực</th>
                                    <th>Thương hiệu</th>
                                    <th>Cập nhật</th>
                                    <th>Xóa</th>
                                </tr>
                            </tfoot>
                             
                        </table>
                    </div>
                            <div class="product-pagination text-center">                                  
                                <ul class="pagination">
                                    <c:forEach var="i" begin="1" end="${pagenumber}">
                                         <li><a href="${pageContext.request.contextPath}/admin/tour/getall.htm?page=${i}">${i}</a></li>
                                    </c:forEach>
                               
                                </ul>                               
                            </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->          
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>